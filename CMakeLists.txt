tdaq_package(NO_HEADERS)  # There is no public interfaces that are needed outside this package

# TODO(cyildiz): This may affect performance, we should see the effect
remove_definitions(-DERS_NO_DEBUG)

tdaq_generate_isinfo(HLTMPPU_ISINFO schema/HLTMPPU_ISInfo.schema.xml
  OUTPUT_DIRECTORY HLTMPPU
  NAMESPACE HLTMP
  CPP_OUTPUT is_cpp_srcs)

tdaq_add_is_schema(schema/HLTMPPU_ISInfo.schema.xml DESTINATION schema)

tdaq_add_library(HLTMPPU
  src/HLTMPPU.cxx
  src/hltmppufactory.cxx
  ${is_cpp_srcs}
  LINK_LIBRARIES tdaq-common::hltinterface Boost::filesystem Boost::chrono is oh ROOT::Hist ROOT::Core HLTMPPU_ISINFO)

tdaq_add_library(DFDataSource
  src/DFDataSource.cxx
  src/dfds_factory.cxx
  INCLUDE_DIRECTORIES dfinterface
  LINK_LIBRARIES tdaq-common::hltinterface Boost Boost::filesystem ROOT::Hist)

tdaq_add_library(DFFileBackend
  src/DFFileSession.cxx
  src/DFFileEvent.cxx
  src/eformat_utils.cxx
  INCLUDE_DIRECTORIES dfinterface
  LINK_LIBRARIES tdaq-common::eformat tdaq-common::eformat_write tdaq-common::DataReader tdaq-common::DataWriter)

tdaq_add_library(DFDcmEmuBackend
  src/DcmEmulator/DFDcmEmuSession.cxx
  src/DcmEmulator/DFDcmEmuEvent.cxx
  INCLUDE_DIRECTORIES dfinterface
  LINK_LIBRARIES Boost tdaq-common::eformat tdaq-common::eformat_write)

tdaq_add_library(HLTMPPUFileReaderWriter
   src/DcmEmulator/FileReaderWriter.cxx
  LINK_LIBRARIES Boost tdaq-common::ers tdaq-common::eformat tdaq-common::eformat_write tdaq-common::DataReader tdaq-common::DataWriter)

tdaq_add_library(HLTMPPUDcmEmulator
  src/DcmEmulator/DcmEmulator.cxx
  src/eformat_utils.cxx
  INCLUDE_DIRECTORIES dfinterface
  LINK_LIBRARIES Boost tdaq-common::ers tdaq-common::eformat tdaq-common::eformat_write HLTMPPUFileReaderWriter)

tdaq_add_executable(hltmppu_dcm_emulator
  src/DcmEmulator/dcm_emulator.cxx
  LINK_LIBRARIES  Boost Boost::program_options HLTMPPUDcmEmulator tdaq-common::ers)

tdaq_add_executable(hltmppu_dcm_requester
  src/DcmEmulator/dcm_requester.cxx
  LINK_LIBRARIES  Boost Boost::program_options DFDcmEmuBackend rt tdaq-common::ers)

tdaq_add_library(MonSvcInfoService
  src/MonSvcInfoService.cxx
  src/MonSvcInfoService_factory.cxx
  LINK_LIBRARIES monsvcpub tdaq-common::hltinterface oh)

tdaq_add_library(MonSvcInfoServiceDummPublisher
  src/MonSvcInfoServiceDummPublisher.cxx
  src/MonSvcInfoServiceDummPublisher_factory.cxx
  LINK_LIBRARIES monsvc monsvcpub tdaq-common::hltinterface oh MonSvcInfoService)

tdaq_add_library(HLTMPPy_boost
  python/HLTMPPy_boost.cxx
  LINK_LIBRARIES Python::Development HLTMPPU HLTMPPUDcmEmulator Boost::python rt dl)

tdaq_add_python_package(HLTMPPy)

tdaq_add_executable(HLTMPPU_main
  src/HLTMPPU_main.cxx
  LINK_LIBRARIES HLTRCCommand Boost::program_options ipc tdaq-common::ers)

tdaq_add_executable(HLTMPPU_test
  test/HLTMPPUTestApp.cxx
  LINK_LIBRARIES Boost::program_options ipc tdaq-common::ers)

tdaq_add_executable(test_dffile
  test/test_dffilebackend.cxx
  LINK_LIBRARIES DFFileBackend Boost tdaq-common::eformat tdaq-common::eformat_write)

tdaq_add_scripts(scripts/runHLTMPPy.py)

tdaq_add_scripts(test/test_runHLTMPPy.py)

# Unit tests
add_test(NAME test_HLTMPPy COMMAND ${TDAQ_RUNNER} ${CMAKE_CURRENT_SOURCE_DIR}/test/test_HLTMPPy.py)

# One has to update the python path, otherwise it won't have the current software
set_tests_properties(test_HLTMPPy PROPERTIES   ENVIRONMENT
  PYTHONPATH=${CMAKE_CURRENT_SOURCE_DIR}/python:$ENV{PYTHONPATH})

tdaq_add_executable(test_monsvcinfoservice NOINSTALL
  test/test_monsvcinfoservice.cxx
  LINK_LIBRARIES MonSvcInfoService Boost::unit_test_framework)
tdaq_add_test(NAME test_monsvcinfoservice COMMAND ./test_monsvcinfoservice POST_INSTALL)
