// Dear emacs, this is -*- c++ -*-
#ifndef HLTMPPU_DFDATASOURCE_H
#define HLTMPPU_DFDATASOURCE_H

#include "HLTMPPU/DataSource.h"
#include <chrono>
#include <cstddef>
#include <mutex>
#include <shared_mutex>
#include <queue>

#include <boost/dll.hpp>
//#include "dfinterface/dfinterface.h"

class TH1F;
class TH2F;

namespace daq{
  namespace dfinterface{
    class Session;
    class Event;
  }
}

namespace HLTMP{

  //! Initializes a histogram for each ROS that contains latency measurement at each collect call
  class ROSLatency {

   private:
    // Fills m_ros_info histograms with total size of retrieved ROBs(bytes) vs time elapsed(microseconds)
    boost::property_tree::ptree m_ros2robtree;
    std::map<std::string,TH2F*> m_ros_histos; //! TH2F Histograms per ROS that holds the information of total bytes received vs time elapsed

    size_t m_max_size_bytes, m_max_duration_us;  //! Histogram limits for latency measurement

    //! Map for quick access to get corresponding ROS of ROB
    std::unordered_map<uint32_t, std::string> m_rob2ros;

    bool m_active;  // If false, this class will not do anything

   public:
    ROSLatency(const boost::property_tree::ptree & pt);
    ~ROSLatency() {}
    void prepareWorker();  //! Initialize histograms
    void fillRosRequestStats(std::vector<hltinterface::DCM_ROBInfo>& data);  //! Fills historgrams
    bool isActive() { return m_active;}

  };

  /*! DataSource implementation used in online (and offline with DcmEmulator).
   *
   *  DFDataSource uses dfinterface to transmit fragments between data source
   *  (such as Dcm or DcmEmulator) and the HLTMPPU. Each data source implements
   *  their own dfinterface::Session and dfinterface::Event. The shared
   *  library is loaded in m_dfinterface_library at prepareWorker
   */
  class DFDataSource:public DataSource{
  public:
    virtual ~DFDataSource();
    DFDataSource();
    virtual bool configure(const boost::property_tree::ptree &args) override;
    virtual bool prepareForRun(const boost::property_tree::ptree &args) override;

    std::string globalIDsAsString();

    virtual hltinterface::DataCollector::Status getNext (std::unique_ptr<uint32_t[]>& l1r) override;
    virtual void eventDone (std::unique_ptr<uint32_t[]> hltr) override;

    virtual uint32_t collect(std::vector<hltinterface::DCM_ROBInfo>& data,
     const uint64_t gid, const std::vector<uint32_t>& ids) override;

    virtual uint32_t collect(std::vector<hltinterface::DCM_ROBInfo>& data,
			     const uint64_t global_id) override;
    virtual void reserveROBData(const uint64_t global_id, const std::vector<uint32_t>& ids) override;

    virtual bool finalize(const boost::property_tree::ptree &args) override;
    virtual bool prepareWorker(const boost::property_tree::ptree &args) override;
    virtual bool finalizeWorker(const boost::property_tree::ptree &args) override;

    //! Return a ptree with information of current and accumulated processing time statistics
    //! This function resets m_currentStats at the end
    virtual boost::property_tree::ptree getStatistics() override;

  private:
    boost::dll::shared_library m_dfinterface_library;  // This is declared before m_sessions, as we want it to be destroyed last
    size_t m_slots;  // Number of event slots in AthenaMT
    uint16_t m_getNextTimeout;

    /* One session for each event slot. On getNext an event (global ID) is
     * associated with a session. The session is "released" in eventDone.
     *
     * Access to m_sessions is not protected: the container is filled in
     * prepareWorker and never modified later. Thus:
     * - prepareWorker can be called only once
     * - prepareWorker must be called before any call to: finalizeSession,
     *   finalize, getNext, and eventDone.
     */
    std::vector<std::unique_ptr<daq::dfinterface::Session>> m_sessions;

    typedef std::map<uint64_t, size_t> TEventSession; // global_id -> session_id
    TEventSession m_event_session; // which session is associated with which event
    std::deque<size_t> m_free_sessions; // sessions available to manage events
    std::shared_mutex m_session_mutex; // access to m_event_session and m_free_sessions
    size_t get_event_session_id(uint64_t);
    size_t get_available_session_id();
    void associate_event_session(uint64_t, size_t);
    void release_session(size_t); // when not yet associated to an event
    void release_session(uint64_t, size_t); // when associated to an event

    std::map<uint64_t, std::unique_ptr<daq::dfinterface::Event>> m_events;  // Map such as: <global_id, event>
    // Mutex to protect access to m_events
    std::shared_mutex m_collectMutex;
    bool knownGid(uint64_t global_id); // Thread safe method that returns true if global_id is in m_events

    // std::map<uint64_t, size_t> m_event_session;  // Map of global_id to session IDs
    boost::property_tree::ptree m_configTree;
    std::string m_dfinterface_library_name;  // Library that implements dfinterface classes
    std::mutex m_statMutex;  // Mutex to protect getNext/eventDone calls.
    bool finalizeSession(const size_t session_id);  // Finalize session by calling tryGetNext
    bool nomoreevents;  // True if any session received nomoreevents

    //! Class that contains the processing time statistics
    class Stats {
     public:
      Stats() {
        reset();
      }
      ~Stats() {}

      uint64_t numEvents;
      uint32_t acceptedEvents;
      uint64_t rejectedEvents;

      std::chrono::duration<double, std::milli> waitDuration, //waiting on L1Result
        sendDuration,   // time to send
        processDuration, // time spend in processing
        acceptDuration,  // time spend in processing before accepting event
        rejectDuration,  // time spend in processing before rejecting event
        totalDuration,  // total time
        longestWaitForL1Result,
        longestProcessingTime;

      // Reset all values to 0
      void reset();

      // Print average, longest and fraction times
      void print();
    };

    //! Time points of a certain event, used to calculate duration of each step
    struct EventTimeInfo {
      std::chrono::time_point<std::chrono::steady_clock> start,  // Event start
        l1,    // l1 results obtained
        proc,  // processing ended
        send,  // hlt results sent
        end;   // Event end
    };

    std::map<uint64_t, EventTimeInfo> m_eventTimes;
    std::mutex m_eventTimesMutex;

    Stats m_currentStats;  // Statistics of current interval, reset every time getStatistics() is called
    Stats m_accumulatedStats;  // Accumulated statistics of the whole run

    std::vector<TH1F*> m_histos;

    std::unique_ptr<ROSLatency> m_ros_latency;  //! ROS Latency measurement is only done if configured
  };
}//namespace
#endif
