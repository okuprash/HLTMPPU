// Dear emacs, this is -*- c++ -*-
#ifndef HLTMPPU_DATASOURCE_H
#define HLTMPPU_DATASOURCE_H

#include <future>
#include <boost/property_tree/ptree.hpp>
#include "hltinterface/DataCollector.h"
#include "eformat/write/FullEventFragment.h"
//#include "hltinterface/HLTResult.h"


namespace HLTMP{

  /*! Base DataSource class
  *  This class extends DataCollector with state transitions and
  *  specific needs for HLTMPPU (such as getStatistics).
  */
  class DataSource:public hltinterface::DataCollector{
  public:
    virtual ~DataSource(){};
    //DataSource()=0;
    virtual bool configure(const boost::property_tree::ptree &args)=0;
    virtual bool prepareForRun(const boost::property_tree::ptree &args)=0;
    virtual Status getNext (std::unique_ptr<uint32_t[]>& l1r) override=0;
    virtual void eventDone (std::unique_ptr<uint32_t[]> hltr) override=0;
    virtual uint32_t collect(std::vector<hltinterface::DCM_ROBInfo>& data,
			     const uint64_t global_id, const std::vector<uint32_t>& ids) override =0;
    virtual uint32_t collect(std::vector<hltinterface::DCM_ROBInfo>& data,
			     uint64_t global_id) override =0;
    virtual void reserveROBData(const uint64_t global_id, const std::vector<uint32_t>& ids)override =0;
    virtual bool finalize(const boost::property_tree::ptree &args)=0;
    virtual bool prepareWorker(const boost::property_tree::ptree &args)=0;
    virtual bool finalizeWorker(const boost::property_tree::ptree &args)=0;

    //! Return event processing time statistics as a ptree, whose format should be agreed between
    //! HLTMPPU and DataSource implementation
    virtual boost::property_tree::ptree getStatistics()=0;
  };
}//namespace
#endif
