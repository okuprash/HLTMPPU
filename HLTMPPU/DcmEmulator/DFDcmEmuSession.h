// Dear emacs, this is -*- c++ -*-
#ifndef DF_DCM_EMU_SESSION_H
#define DF_DCM_EMU_SESSION_H

#include <mutex>
#include <utility>

#include "ers/ers.h"
#include "eformat/eformat.h"
#include "eformat/index.h"
#include "eformat/write/eformat.h"
#include "eformat/SourceIdentifier.h"
#include "eformat/FullEventFragmentNoTemplates.h"

#include <boost/interprocess/shared_memory_object.hpp>

#include "dfinterface/dfinterface.h"

#include "HLTMPPU/DcmEmulator/dcm_emulator_shared_data.h"

namespace HLTMP{
  class DFDcmEmuSession:public daq::dfinterface::Session{

  public:
    DFDcmEmuSession(const boost::property_tree::ptree &args);
    /*! \brief Opens the Session.
     *
     *  \param[in] clientName Name of the application opening the Session. The application name
     *      must be suitable to report a misbehaving application to the Run Control infrastructure.
     *
     *  \exception boost::system::system_error is thrown if the Session can not be created due to
     *      communication issues with the service, e.g. the service is unreachable or does not
     *      respond 
     *      (<a href="http://www.boost.org/doc/libs/1_53_0/libs/system/doc/reference.html#Class-system_error">boost::system::system_error</a>)
     */
    virtual void open(const std::string& clientName) override;

    /*! \brief Determines whether the Session is open.
     */
    virtual bool isOpen() const override;

    /*! \brief Closes the Session.
     */
    virtual void close() override;

    /*! \brief Returns a pointer to the next \ref Event object to be processed.
     *
     *  Blocks indefinitely while waiting for new events.
     *
     *  \exception NoMoreEvents is thrown if there are no more events to be processed.
     *  \exception boost::system::system_error is thrown in case of network communication issues
     *      (<a href="http://www.boost.org/doc/libs/1_53_0/libs/system/doc/reference.html#Class-system_error">boost::system::system_error</a>)
     */
    virtual std::unique_ptr<daq::dfinterface::Event> getNext() override;

    /*! \brief Returns a pointer to the next \ref Event object to be processed.
     *
     *  Blocks until an event can be obtained, or the specified time is reached.
     *
     *  \exception OperationTimedOut is thrown if an event could not be obtained before \c absTime.
     *  \exception NoMoreEvents is thrown if there are no more events to be processed.
     *  \exception boost::system::system_error is thrown in case of network communication issues 
     *      (<a href="http://www.boost.org/doc/libs/1_53_0/libs/system/doc/reference.html#Class-system_error">boost::system::system_error</a>)
     */
    virtual std::unique_ptr<daq::dfinterface::Event> tryGetNextUntil(
						   const std::chrono::steady_clock::time_point& absTime) override;

    /*! \brief Returns a pointer to the next \ref Event object to be processed.
     *
     *  Equivalent to:
     *  \code
     *  tryGetNextUntil(std::chrono::steady_clock::now() + relTime)
     *  \endcode
     */
    virtual std::unique_ptr<daq::dfinterface::Event> tryGetNextFor(
						 const std::chrono::steady_clock::duration& relTime) override;

    /*! \brief Marks the event as accepted by the High-Level Trigger.
     *
     *  \param[in] event An \ref Event object obtained from getNext(), tryGetNextUntil() or
     *      tryGetNextFor()
     *  \param[in] hltr Points to serialized read::FullEventFragment, it should contain High Level
     *      trigger information, streamTags, pscErrors and HLT fragments
     *
     *  \exception boost::system::system_error is thrown in case of network communication issues 
     *      (<a href="http://www.boost.org/doc/libs/1_53_0/libs/system/doc/reference.html#Class-system_error">boost::system::system_error</a>)
     *
     */
    virtual void accept(std::unique_ptr<daq::dfinterface::Event> event,
			std::unique_ptr<uint32_t[]> hltr) override;

    /*! \brief Marks the event as rejected by the High-Level Trigger.
     *
     *  \param[in] event An \ref Event object obtained from getNextEvent(),
     *      tryGetNextEventUntil() or  tryGetNextEventFor()
     *
     *  \exception boost::system::system_error is thrown in case of network communication issues 
     *      (<a href="http://www.boost.org/doc/libs/1_53_0/libs/system/doc/reference.html#Class-system_error">boost::system::system_error</a>)
     */
    virtual void reject(std::unique_ptr<daq::dfinterface::Event> event) override;

    //! Adds the information of ROS Hits by checking which ROBID is from which ROS
    //! \param robid_size Retrieved ROBs, where each element is {robid, size in bytes}
    //! \param getall true if information is sent from getAllRobs, false if it's from getRobs
    void addRosHitInfo(std::vector<std::pair<uint32_t,double>> robid_size, bool getall);

    //! Adds the information of robs to be prefetched
    //! \param robid_size ROBs to be prefetched, where each element is {robid, size in bytes}
    void addPrefetchInfo(std::vector<std::pair<uint32_t,double>> robid_size);

    //! Update ROB cache with prefetched ROBs 
    //! \param robid id of ROB which gets retrieved from ROS
    //! \param robCache list of ROBs which have been already retrieved, list will be augmented with the ROBs which get prefetched with robid
    void updateRobCacheInfo(const uint32_t & robid, std::set<uint32_t> & robCache);

    virtual ~DFDcmEmuSession();
  private:
    std::unique_ptr<uint32_t[]> getNextEvent();

    std::unique_ptr<boost::interprocess::shared_memory_object> m_shm;

    std::unique_ptr<boost::interprocess::mapped_region> m_region;

    // This class communicates with DCM via this object
    session_info * m_session_info;

    // Name of session, same as the name of shared memory
    std::string m_name;

    std::vector<uint32_t> m_extraL1Robs;  // List of extra ROBs that should be retrieved in L1 results

    /*! Class to keep roshit information for a single event.
     *
     * This class only calculates statistics, and doesn't do any ROB retrieval.
     * The ROB retrievals are done directly in DFDcmEmuEvent who has access to
     * the full event. This class, use the information of getRobs, getAllRobs
     * and mayGetRobs calls, and calculates how many time each ROS is accesses,
     * how many ROBs are requested in each ROS and total size of retrieved ROBs.
     *
     * ROSHits class holds information of a single event, and reset at each event.
     *
     */
    class ROSHits {

     private:
     // Hits per ROS
     struct Hits {
       uint32_t ros;  // Number of ros hits
       uint32_t rob;  // Number of requested ROBs
       double size;  // Size of retrieved ROBs in bytes
     };

     //! Statistics of each ROS for Event::getRobs calls
     std::map<std::string, Hits> m_roshits_getRobs;

     //! Statistics of each ROS for Event::getAllRobs calls
     std::map<std::string, Hits> m_roshits_getAllRobs;

     //! Map for quick access to get corresponding ROS of ROB
     std::unordered_map<uint32_t, std::string> m_rob2ros;

     //! Map containing ROBID - Size of ROB fragment in bytes
     using PrefetchMap = std::map<uint32_t, double>;

     //! Information of ROS have ROBS to be prefetched at the next call to the ros
     //!
     //! Key is ROS name and value is map of ROBID-size
     //! At every DFDcmEmuEvent::mayGetRobs call, this map is populated
     std::map<std::string, PrefetchMap> m_robs_tobe_prefetched;

     //! Information of ROS have prefetched ROBS
     //!
     //! At any getRobs,getAllRobs call, it checks if the ROSes that are accessed
     //! are already in m_robs_tobe_prefetched list, and element for that ROS is
     //! moved to this map
     std::map<std::string, PrefetchMap> m_robs_prefetched;

     public:
      /*! Create rob2ros map, initialize m_roshits with all ROS names
       *
       *  \param ros2robs ptree that contains ros-rob mapping
       */
      ROSHits(const boost::property_tree::ptree & ros2robs);
      ~ROSHits() {};

      //! Clear all containers other than m_rob2ros
      void clear();

      //! Add new statistics to m_roshits_getRobs and m_roshits_getAllRobs using ROS2ROB mapping
      //! \param rob_size Retrieved ROBs, where each element is {robid, size in bytes}
      //! \param getall true if information is sent from getAllRobs, false if it's from getRobs
      void add(std::vector<std::pair<uint32_t, double>> rob_size, bool getall);

      //! Record information of ROBs into m_robs_tobe_prefetched using ROS2ROB mapping
      //! \param rob_size ROBs to be prefetched, where each element is {robid, size in bytes}
      void prefetch(std::vector<std::pair<uint32_t, double>> rob_size);

      //! Convert statistics to a json string
      std::string to_json_string();

      //! Get list of ROBs which should be prefetched with a given ROB which gets retrieved from the ROS
      //! \param robid id of ROB which needs to be retrieved from a ROS
      //! \param robs2prefetch ROBs which should be prefetched togehter with robid 
      void getROBs2prefetch(const uint32_t & robid, std::set<uint32_t> & robs2prefetch);
    };

    ROSHits m_roshits;
    std::mutex m_stat_mutex;  // Protect access to m_roshits
  };
}
#endif
