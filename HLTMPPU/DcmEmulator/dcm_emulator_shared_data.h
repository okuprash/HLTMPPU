
#ifndef HLTMPPU_DCM_EMULATOR_SHARED_DATA_H_
#define HLTMPPU_DCM_EMULATOR_SHARED_DATA_H_

#include "HLTMPPU/DcmEmulator/SharedMemoryUtils.h"
#include "ers/ers.h"

#include <string>
#include <sstream>


#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>

//! Object to hold information for a single event
struct event_info {
  //! Event global ID
  uint64_t globalID{0};

  //! ROS Statistics in form of a json tree
  char ros_stats[100000];

  //! Final decision to accept/reject event
  bool accept{false};

  void print() const {
    std::cout << tostring();
  }

  std::string tostring() const {
    std::ostringstream oss;
    oss << "Event Shared Memory:" <<"\n"
        << "  globalID         : " << globalID << "\n"
        << "  accept?          : " << accept << "\n";

    return oss.str();
  }
};

//! Object to hold information for a single session
struct session_info {
  enum { LineSize = 1000 };

  ~session_info() {
    ERS_DEBUG(3, "Destroying session_info instance: " << shared_name);
  }

  // True if DCM is ready to process a request for new event
  bool dcm_listening{false};

  // True when new event was placed in the fullev_shared_vec and awaits being read
  bool new_event_available{false};

  // True if worker is processing an event
  bool processing_event{false};

  // True if no more events
  bool nomoreevents{false};

  //! Name of the shared memory object where event data will be stored
  //! This should be a unique name containing session_name-global_id
  char shared_name[LineSize];

  //! Shared vector handler for full event
  std::unique_ptr<HLTMP::SharedVectorHandler> fullev_shared_vec;

  //! Shared vector handler for hlt result
  std::unique_ptr<HLTMP::SharedVectorHandler> hltres_shared_vec;

  //! Mutex to protect access to the queue
  boost::interprocess::interprocess_mutex mutex;

  //! Condition to signal new event was requested to be placed in shared memory
  boost::interprocess::interprocess_condition  cond_next_requested;

  //! Condition to signal new event was provided and is available in shared memory
  boost::interprocess::interprocess_condition  cond_next_provided;

  //! Condition to signal finished event in shared memory is ready for output processing
  boost::interprocess::interprocess_condition  cond_output_processing_requested;

  //! Condition to signal output processing for a finished event was completed
  boost::interprocess::interprocess_condition  cond_output_processing_finished;

  //! Current event information
  event_info ev_info;

  void print() const {
    std::cout << tostring();
  }

  std::string tostring() const {
    std::ostringstream oss;
    oss << "Session Shared Memory:  " << "\n"
        << "  dcm_listening  : " << dcm_listening << "\n"
        << "  shared_name  : " << shared_name << "\n"
        << "  globalID     : " << ev_info.globalID << "\n"
        << "  nomoreevents : " << nomoreevents << "\n";
    return oss.str();
  }
};

#endif  // HLTMPPU_DCM_EMULATOR_SHARED_DATA_H_
