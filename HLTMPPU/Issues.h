#ifndef HLTMPPU_ISSUES_H
#define HLTMPPU_ISSUES_H

ERS_DECLARE_ISSUE(HLTMPPUIssue,                // namespace name
		  CommandLineIssue,   // issue name
		  "Command-line parameter issue: " << type << ".",        // message
		  ((const char *)type )             // first attribute
		  )

ERS_DECLARE_ISSUE(HLTMPPUIssue,                // namespace name
		  DLLIssue,   // issue name
		  "Can't load dll: \"" << type << "\".",        // message
		  ((const char *)type )             // first attribute
		  )

ERS_DECLARE_ISSUE(HLTMPPUIssue,                // namespace name
		  UnexpectedIssue,   // issue name
		  "Unexpected Issue: \"" << type << "\".",        // message
		  ((const char *)type )             // first attribute
		  )

ERS_DECLARE_ISSUE(HLTMPPUIssue,                // namespace name
		  ConfigurationIssue,   // issue name
		  "Configuration Issue: \"" << type << "\".",        // message
		  ((const char *)type )             // first attribute
		  )

ERS_DECLARE_ISSUE(HLTMPPUIssue,                // namespace name
		  TransitionIssue,   // issue name
		  "Transition Issue: \"" << type << "\".",        // message
		  ((const char *)type )             // first attribute
		  )

ERS_DECLARE_ISSUE(HLTMPPUIssue,                // namespace name
		  ChildIssue,   // issue name
		  "Child Issue: \"" << type << "\".",        // message
		  ((const char *)type )             // first attribute
		  )

#endif
