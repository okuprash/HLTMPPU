// Dear emacs, this is -*- c++ -*-
#ifndef HLTMPPU_MONSVCINFOSERVICEDUMMPUBLISHER_H
#define HLTMPPU_MONSVCINFOSERVICEDUMMPUBLISHER_H
//#include "HLTMPPU/InfoService.h"
#include "hltinterface/IInfoRegister.h"
#include <map>
#include <set>
#include <vector>
#include <string>
#include <memory>
#include <atomic>
#include "boost/thread/thread.hpp"
#include <unordered_map>
#include <unordered_set>
#include "monsvc/ptr.h"
#include "HLTMPPU/MonSvcInfoService.h"

namespace monsvc{
  class PublishingController;
  class ConfigurationRule;
}
namespace boost{
  class thread;
}
namespace hltinterface{
  class GenericHLTContainer;
  class ContainerFactory;
}
class ISInfoDynAny;
class IPCPartition;
class TH1;

namespace HLTMP{
  
  class MonSvcInfoServiceDummPublisher:public HLTMP::MonSvcInfoService{
    using ContPtr = std::shared_ptr<hltinterface::GenericHLTContainer>;
  public:
    ~MonSvcInfoServiceDummPublisher();
    MonSvcInfoServiceDummPublisher();
    bool configure(const boost::property_tree::ptree &args) override;
    bool prepareWorker(const boost::property_tree::ptree &args) override;
    bool finalizeWorker(const boost::property_tree::ptree &args) override;
    bool finalize(const boost::property_tree::ptree &args) override;
    static IInfoRegister* getInstance();
  private:
    TH1* createHisto(const boost::property_tree::ptree &args,int type);
    void parseHistogramDataFile(const std::string &path);
    void fillHistos();
    void startFiller();
    void stopFiller();
    boost::thread *m_fillerThread;
    int  m_fillBin;
    unsigned int m_fillPeriod;
    std::atomic<bool> m_fillerWork;
    std::vector<TH1*> m_dummHistos;    
  };
}//end namespace

#endif
