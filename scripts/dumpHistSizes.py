#!/bin/env python

from __future__ import print_function
from builtins import str
from builtins import range
import sys
import xml.etree.ElementTree as et
import pprint
count=0
sumBuffFile=0
sumKey=0
sumComp=0
sumNeeded=0
sumWithErrs=0
dirDict={}
def dumpAxis(axis,axisName,obj):
    labelsList=axis.GetLabels()
    nBinsX=axis.GetNbins()
    ax=et.Element(axisName)
    et.SubElement(ax,"Nbins").text=str(nBinsX)
    et.SubElement(ax,"Min").text=str(axis.GetXmin())
    et.SubElement(ax,"Max").text=str(axis.GetXmax())
    if labelsList:
        binlabels=et.Element("BinLabels")
        ax.append(binlabels)
        for i in range(1,nBinsX):
            bin=et.Element("bin",num=str(i))
            bin.text=axis.GetBinLabel(i)
            binlabels.append(bin)        
    del labelsList
    return ax

def dump1D(key,path):
    el=et.Element(key.GetClassName())
    et.SubElement(el,"Name").text=key.GetName()
    et.SubElement(el,"Path").text="/".join(path)
    obj=key.ReadObj()
    Title=obj.GetTitle()
    et.SubElement(el,"Title").text=Title
    xaxis=obj.GetXaxis()
    el.append(dumpAxis(xaxis,"XAxis",obj))
    obj.Delete()
    del obj
    return el

def dump2D(key,path):
    el=et.Element(key.GetClassName())
    et.SubElement(el,"Name").text=key.GetName()
    et.SubElement(el,"Path").text="/".join(path)
    obj=key.ReadObj()
    Title=obj.GetTitle()
    et.SubElement(el,"Title").text=Title
    xaxis=obj.GetXaxis()
    el.append(dumpAxis(xaxis,"XAxis",obj))
    xaxis=obj.GetYaxis()
    el.append(dumpAxis(xaxis,"YAxis",obj))
    obj.Delete()
    del obj
    return el

def dump3D(key,path):
    el=et.Element(key.GetClassName())
    et.SubElement(el,"Name").text=key.GetName()
    et.SubElement(el,"Path").text="/".join(path)
    obj=key.ReadObj()
    Title=obj.GetTitle()
    et.SubElement(el,"Title").text=Title
    xaxis=obj.GetXaxis()
    el.append(dumpAxis(xaxis,"XAxis",obj))
    xaxis=obj.GetYaxis()
    el.append(dumpAxis(xaxis,"YAxis",obj))
    xaxis=obj.GetZaxis()
    el.append(dumpAxis(xaxis,"ZAxis",obj))
    obj.Delete()
    del obj
    return el

def dumpProfile(key,path):
    pass
funcDict={"3":dump3D,"2":dump2D,"1":dump1D}

def _dolsrwrapper(fname):
    import ROOT
    rf = ROOT.TFile.Open(fname, 'READ')
    if not rf or not rf.IsOpen():
        print('   %s is empty or not accessible' % f)
        return False

    cleancache = ROOT.gROOT.MustClean(); ROOT.gROOT.SetMustClean(False)
    root=et.Element("HistDump")
    _dolsr(rf,root)
    histdump=open("histDump.xml",'wb')
    ewriter=et.ElementTree(root)
    ewriter.write(histdump,pretty_print=True,xml_declaration=True,encoding="utf-8")
    rf.Close()
    ROOT.gROOT.SetMustClean(cleancache)

def _dolsr(dir,xmlroot):
    import ROOT
    global count
    global sumBuffFile
    global sumKey
    global sumComp
    global dirDict
    buff=ROOT.TBufferFile(ROOT.TBufferFile.kWrite)
    keys = dir.GetListOfKeys()
    currPath=dir.GetPath()
    print("before",currPath)
    currPath="".join(str.split(currPath,"root:")[1:])
    print("after",currPath)
    dirName=dir.GetName()
    if False:# "run_284285" in currPath:
        path=[""]+str.split(currPath,"/")[5:]
    else:
        path=[""]+str.split(currPath,"/")[3:]
    dirpath=currPath
    if dirpath not in dirDict:
        dirDict[dirpath]=[]
    objlist=dirDict[dirpath]
    print("path list",str.split(currPath,"/"))
    for key in keys:
        name = key.GetName()
        print("Reading key named %s at path %s "%(name,dirpath))
        className=key.GetClassName()
        keyClass=ROOT.TClass.GetClass(className)
        if keyClass.InheritsFrom('TH1'):
            if name in objlist: continue
            objlist.append(name)
            e=None
            if "TH" in className:
                e=funcDict[className[2]](key,path)
            elif className=="TProfile":
                e=funcDict["1"](key,path)
            else:
                e=funcDict[className[8]](key,path)
            # if className[2]=="3": 
            #     e=dump3D(key,path)
            # elif className[2]=="2": 
            #     e=dump2D(key,path)
            # elif className[2]=="1": 
            #     e=dump1D(key,path)
            # else:
            #     e=dump1D(key,path)
            #print(et.tostring(e,pretty_print=True))
            xmlroot.append(e)
            continue
            count+=1
            currObj=key.ReadObj()
            buff.Reset()
            currObj.Streamer(buff)
            sumKey+=key.GetObjlen()
            sumBuffFile+=buff.Length()
            sumComp+=key.GetNbytes()
            nBinsX=currObj.GetNbinsX()
            nBinsY=currObj.GetNbinsY()
            nBinsZ=currObj.GetNbinsZ()
            nBins=(nBinsX)*(nBinsY)*(nBinsZ)
            unitSize=1
            unitWerr=2
            if keyClass.InheritsFrom('TArrayS') : 
                unitSize=2
                unitWerr=6
            elif keyClass.InheritsFrom('TArrayI') : 
                unitSize=4
                unitWerr=12
            elif keyClass.InheritsFrom('TArrayF') : 
                unitSize=4
                unitWerr=12
            elif keyClass.InheritsFrom('TArrayD') : 
                unitSize=8
                unitWerr=16
                if keyClass.InheritsFrom('TProfile') :
                    unitSize=16
                elif keyClass.InheritsFrom('TProfile2D') :
                    unitSize=16
                elif keyClass.InheritsFrom('TProfile3D') :
                    unitsize=16
            needed=unitSize*nBins
            neededWerr=unitWerr*nBins
            print(key.GetClassName()+"\t"+key.GetName()+"\tnbins="+repr(nBins)+"\tX=%s\tY=%s\tZ=%s"%(\
                nBinsX,nBinsY,nBinsZ,\
                    needed,neededWerr))
            currObj.Delete()
        if keyClass.InheritsFrom('TDirectory'):
            # is a check? is a summary? or recurse?
            #if ("run_215456" == dirName): continue
            dirobj = key.ReadObj()
            if ("Histogramming" in dirpath or "Histogramming" in name):
                _dolsr(dirobj,xmlroot)
                dirobj.Delete()
            if ("run_284285" == dirpath) and (name != "lb_1111"):
                _dolsr(dirobj,xmlroot)
                dirobj.Delete()
            if ("Top-MIG" in dirpath or "Top-MIG" in name):
                _dolsr(dirobj,xmlroot)
                dirobj.Delete()
                
            # if name[-1] == '_' and resultdir:
            #     hist = dir.Get(name[:-1])
            #     subkeys = resultdir.GetListOfKeys()
            #     for subkey in subkeys:
            #         ressub = subkey.GetName()
            #         if ressub not in ('Status', 'Reference', 'ResultObject') and ROOT.TClass.GetClass(subkey.GetClassName()).InheritsFrom('TDirectory'):
            #             l = subkey.ReadObj().GetListOfKeys()
            #             l[0].GetName()

            #     # status flag
            #     l = statusobj.GetListOfKeys()
            #     l[0].GetName()
            else:
                continue
        else:
            print("Object type key Class=%s"%key.GetClassName())
            currObj=key.ReadObj()
            if currObj == None:
                print("WARNING Object \"%s\" in file:directory \"%s\" is corrupt "\
                "keylen=%s numbytes=%s objlen=%s fseekkey=%s"%(name,dir.GetPath(),key.GetKeylen(),
                                                              key.GetNbytes(),key.GetObjlen(),key.GetSeekKey()))
            currObj.Delete()
            del currObj

                
def scanFiles(globstr='testOutFile*'):
    import glob
    for i in glob.glob(globstr):
        print("-"*40,"Scanning file",i,"-"*60)
        _dolsrwrapper(i)

if __name__ == "__main__" :
    if len(sys.argv)<2:
        scanFiles()
    else:
        scanFiles("%s*"%(sys.argv[1]))
    print("Counted ",count," histograms. Size from Keys compressed=",sumComp,"uncompressed=",sumKey,"from buffer=",sumBuffFile)

