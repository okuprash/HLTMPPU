#!/usr/bin/env tdaq_python
# A simple script to publish fake magnetic field current values to
# DCS_GENERAL IS server. Server needs to be present in the inital partition
# Simplest solution to start server from the console
# Can be modified to publish different type of objects to different servers easily
# 2015 Sami Kama

from ipc import IPCPartition
from ispy import *
p=IPCPartition('initial')# where the IS server resides
objSolenoid = ISObject(p,'DCS_GENERAL.MagnetSolenoidCurrent.value', 'DdcFloatInfo') #partition, ServerName+objectName, IS Object type
objToroid=ISObject(p,'DCS_GENERAL.MagnetToroidsCurrent.value', 'DdcFloatInfo') 
objSolenoid.value=31415.9 #set object field
objToroid.value=3.14159 # set object filed
objSolenoid.checkin(True) #publish
objToroid.checkin(True)

