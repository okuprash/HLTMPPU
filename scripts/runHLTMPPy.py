#!/usr/bin/env tdaq_python
# Standalone program to run HLTMPPU
import sys
import argparse
import collections.abc

def arg_eval(s):
   """Argument handler for python types (list, dict, ...)"""
   from ast import literal_eval
   return literal_eval(s)

def parse_extra(parser,extra):
    namespaces=[]
    print("parsing commandline '%s'"%extra)
    while extra:
        n,extra = parser.parse_known_args(extra)
        namespaces.append(n)
        print("Namespace={},extra={}".format(n, extra))
    return namespaces

#from http://stackoverflow.com/questions/20094215/argparse-subparser-monolithic-help-output

class _BigHelp(argparse._HelpAction):
    def __call__(self, parser, namespace, values, option_string=None,ErrReason=None):
        parser.print_help()

        modGroups={' Data Source Modules ':['dcmds','dffileds'],
                   ' Monitoring Modules ':['monsvcis'],
                   ' Trigger Configuration Modules ':['joboptions','DBPython','DB','pudummy'],
                   ' HLTMPPU Module ':['HLTMPPU'],
                   'help':['-h','--help']
               }
        modMaps={}
        for mg in list(modGroups.keys()):
            for m in modGroups[mg]:
                modMaps[m]=mg
        helpGroups={}
        # retrieve subparsers from parser
        subparsers_actions = [
            action for action in parser._actions
            if isinstance(action, argparse._SubParsersAction)]
        # there will probably only be one subparser_action,
        # but better save than sorry
        for subparsers_action in subparsers_actions:
            # get all subparsers and print help
            for choice, subparser in list(subparsers_action.choices.items()):
                hg=modMaps[choice]
                if hg not in helpGroups:
                    helpGroups[hg]=[]
                helpGroups[hg].append("%s\n%s"%('{:-^40}'.format(" %s "%choice),subparser.format_help()))
        print(20*"*" + "MODULES HELP" + 20*"*")
        print(" You can specify --help after module name to see only relative modules help")
        for g in helpGroups:
            print ("\n{:*^60}\n".format(g))
            for m in helpGroups[g]:
                print(m)
        if ErrReason is not None:
            print("\n%s\n"%ErrReason)
        parser.exit()

def getRos2RobDictionary(ros2rob):
    """ Converts ros2rob argument to a dictionary """

    # ros2rob can be either file or a python dictionary
    try:
        with open(ros2rob) as f:
            ros2rob = f.read()  # If file exists, replace ros2rob with content of file
            print("Reading ros2rob from a file")
    except IOError:
        pass

    from ast import literal_eval
    try:
        ros2robdict = literal_eval(ros2rob)
        if type(ros2robdict) is not dict:
            raise(ValueError)
        return ros2robdict
    except Exception:
        sys.stderr.write("ERROR! ros2rob can't be evaluated of it is not a proper python dictionary: {}\n".format(ros2rob))
        sys.stderr.write("Using empty ros2rob dictionary instead\n")
        return {}

def update_nested_dict(basedict, newdict):
   """Update nested dictionary (https://stackoverflow.com/q/3232943)"""
   for k, v in newdict.items():
      if isinstance(v, collections.abc.Mapping):
         basedict[k] = update_nested_dict(basedict.get(k, {}), v)
      else:
         basedict[k] = v
   return basedict

def getConfigDictionary(NamespaceList,modMap):
    d={}
    globalArgs=['extra','log_root','partition_name','schema_files','with_infrastructure','run_number','save_options',
                'options_file','detector_mask','toroid_current','solenoid_current','date','ros2robs', 'cfgdict'
    ]
    for n in NamespaceList:
        gk=modMap[n.module]
        args=vars(n)
        print (args)
        d[gk]={key:value for (key,value) in list(args.items()) if key not in globalArgs}
    if d['trigger']['module'] != 'joboptions':
        d['trigger']['library'].append("TrigConfigSvc")
    if d['trigger']['module'] == 'pudummy':
        d['trigger']['library'] = ['pudummy']
    d['trigger']['logLevels'] = ["INFO","ERROR"]
    ht=d['HLTMPPU']
    ht['log_root']=NamespaceList[0].log_root
    ht['partition_name']=NamespaceList[0].partition_name
    d['global']={key:value for (key,value) in list(vars(NamespaceList[0]).items()) if key in globalArgs[1:]}

    d['global']['ros2robs'] = getRos2RobDictionary(d['global']['ros2robs'])

    # Set a default file if none (necessary to be able to run the script without any arguments
    if d['datasource']['file'] == []:
        d['datasource']['file'] = ["/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/TrigP1Test/data18_13TeV.00364485.physics_EnhancedBias.merge.RAW._lb0705._SFO-1._0001.1"]

    update_nested_dict(d,d["global"].get("cfgdict"))
    d["global"].pop("cfgdict")  # This is not needed anymore
    return d

def main():
    p=argparse.ArgumentParser(description="HLTMPPU python based steering",formatter_class=argparse.ArgumentDefaultsHelpFormatter,add_help=False)
    p.add_argument('-h',"--help",action=_BigHelp,help="Print this help and exit")
    subp=p.add_subparsers(help="Module configurations",dest='module')
    #subp.required=True
    #p.add_argument('--file',nargs='+')
    # monmods=p.add_subparsers(help='Monitoring modules')
    # trigmods=p.add_subparsers(help='Trigger configuration modules')

    dcmdsopts=subp.add_parser('dcmds',help="DCMDataSource options",formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    #dcmdsopts.add_argument('--dslibrary',nargs=1,const="dfinterfaceDcm",default="dfinterfaceDcm")
    dcmdsopts.add_argument('--dslibrary',nargs=1,default="dfinterfaceDcm",help="Library that provides the interface")

    dffileds=subp.add_parser('dffileds',help="File based DF data source options, DcmEmulator or File. "
                             "By default, DcmEmulator is used, one can run with multiple forks/slots. "
                             "To use DFFileBackend, use 1 fork, 1 slot and set dslibrary accordingly.",
                             formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    dffileds.add_argument('--dslibrary',nargs='?',const="DFDcmEmuBackend",default="DFDcmEmuBackend",help="Library that provides the interface.")
    dffileds.add_argument('--loopFiles',action='store_true',help="Whether to loop over the files")
    dffileds.add_argument('--skipEvents',type=int,nargs='?',help="Number of events to skip",const=0,default=0)
    dffileds.add_argument('--numEvents',type=int,nargs='?',help="Number of events to process",const=-1,default=-1)
    dffileds.add_argument('--file',action='append',help="list of files to process, can be repeated for multiple files",
                          default=[])
    dffileds.add_argument('--preload',action='store_true',help="Preload files into memory")
    dffileds.add_argument('--outFile',help="base name of the output file",default="")
    dffileds.add_argument('--rosHitStatFile',help="base name of the ros hit statistics output file",default="ros_hitstats")
    dffileds.add_argument('--compressionLevel',type=int,help="compression level of output file",default=2)
    dffileds.add_argument('--compressionFormat',choices=["ZLIB","UNCOMPRESSED"],help="compression level of output file",default="ZLIB")
    dffileds.add_argument('--extraL1Robs',action='append',default=[],help="Additional ROB IDs that should be considered part of the level 1 result, can be repeated multiple times")

    monsvcis=subp.add_parser('monsvcis',help="MonSvc (online) based monitoring",formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    monsvcis.add_argument('--library',nargs='?',
                          default='MonSvcInfoService',
                          help="Name of monitoring service library",dest='library')
    monsvcis.add_argument('--histogram-server',nargs='?',
                          default='Histogramming',
                          help="Name of destination IS server for Histograms",dest='OHServerName')
    monsvcis.add_argument('--hist-publish-period',nargs='?',type=int,const=80,
                          default=80,help="Publication period for histograms",
                          dest='OHInterval'
                          )
    monsvcis.add_argument('--histogram-include-regex',nargs='?',const='.*',default='.*',
                          help='Histogram include regex',dest="OHInclude")
    monsvcis.add_argument('--histogram-exclude-regex',nargs='?',const='',default='',
                          help='Histogram exclude regex',dest="OHExclude")

    monsvcis.add_argument('--hist-slots',nargs='?',type=int,const=8,default=8,
                          help="Number of slots for OH publication",dest='OHSlots')
    monsvcis.add_argument('--is-publish-period',nargs='?',type=int,const=10,default=10,
                          help="Publication period for IS objects",dest='ISInterval')
    monsvcis.add_argument('--is-server',nargs='?',const='${TDAQ_IS_SERVER=DF}',
                          default='${TDAQ_IS_SERVER=DF}',help="Destination IS server",
                          dest='ISServer')
    monsvcis.add_argument('--is-slots',nargs='?',type=int,const=1,default=1,
                          help="Number of slots for IS publication",dest='ISSlots')
    monsvcis.add_argument('--is-regex',nargs='?',const='.*',default='.*',
                          help='Histogram regex',dest="ISRegex")

    trigcommon=argparse.ArgumentParser(add_help=False)
    trigcommon.add_argument('--library',action='append',default=["TrigServices","TrigPSC"])
    trigcommon.add_argument('--joType',help="JobOptions service type",default="JobOptionsSvc")
    trigcommon.add_argument('--msgType',help="Message service type",default="TrigMessageSvc")

    dbcommon=argparse.ArgumentParser(add_help=False)
    dbcommon.add_argument('--SMK',type=int,help="Super Master Key",default=0)
    dbcommon.add_argument('--l1PSK',type=int,help="Level-1 Prescale key",default=0)
    dbcommon.add_argument('--l1BG',type=int,help="Level-1 Bunch Group key",default=0)
    dbcommon.add_argument('--HLTPSK',help='HLT Prescale key',default=0)
    dbcommon.add_argument('--db-alias',help='Alias for Trigger DB configuration',default="TRIGGERDB")
    dbcommon.add_argument('--use-coral',
                          action='store_true',help='Whether to use local coral proxy')
    dbcommon.add_argument('--coral-server',nargs='?',help='Coral Server url',const='LOCAL_HOST',default='LOCAL_HOST')
    dbcommon.add_argument('--coral-port',nargs='?',type=int,help='Coral Server port',const=3320,default=3320)
    dbcommon.add_argument('--coral-user',nargs='?',help='Coral Server user name',
                          const="ATLAS_CONF_TRIGGER_RUN2_R",
                          default="ATLAS_CONF_TRIGGER_RUN2_R")
    dbcommon.add_argument('--coral-password',nargs='?',help='Coral Server password',
                          const="TrigConfigRead2015",
                          default="TrigConfigRead2015",
                          )

    trigpycommon=argparse.ArgumentParser(add_help=False)
    trigpycommon.add_argument('--precommand',action='append',default=[], help='pre-command, can be repeated')
    trigpycommon.add_argument('--postcommand',action='append',default=[], help='post-command, can be repeated')

    jotrigConf=subp.add_parser('joboptions',help="Joboptions based trigger config",parents=[trigcommon,trigpycommon],
                               formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    jotrigConf.add_argument('--l1MenuConfig',nargs='?',const='DB',default='DB',choices=["DB"]
                            ,help="Where to get L1 menu configuration")
    jotrigConf.add_argument('--joFile',help="Joboptions file to run",default="TrigExMTHelloWorld/MTHelloWorldOptions.py")
    jotrigConf.add_argument('--pythonSetupFile',default="TrigPSC/TrigPSCPythonSetup.py", help='python setup file')

    pudummyConf=subp.add_parser('pudummy',help="pudummy based trigger config(pure tdaq setup without athena)",parents=[trigcommon],
                                formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    pudummyConf.add_argument('--probability', type=float ,help='Probability to accept events (0.0-1.0)', default=0.5)

    DBPyConf=subp.add_parser("DBPython",help="DBPython based trigger config",parents=[trigcommon,dbcommon,trigpycommon],
                             formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    DBPyConf.add_argument('--pythonSetupFile',default="TrigPSC/TrigPSCPythonDbSetup.py", help='python setup file')

    puConf=subp.add_parser("HLTMPPU",help="HLTMPPU Configuration",formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    puConf.add_argument("-N",'--application-name',nargs='?',const="HTLMPPy-1",default="HLTMPPy-1",help="Application Name")
    puConf.add_argument("-i",'--interactive',action='store_true',help="Whether to run in interactive mode")
    puConf.add_argument("-f",'--num-forks',nargs='?',type=int,const=2,default=2,help="Number of children to fork")
    puConf.add_argument("-s",'--num-slots',nargs='?',type=int,const=2,default=2,help="Number of AthenaMT event slots")
    puConf.add_argument("-a",'--num-threads',nargs='?',type=int,const=2,default=2,help="Number of AthenaMT threads")
    puConf.add_argument('-F','--finalize-timeout',type=int,nargs=1,default=120,help="Timeout at finalization")
    puConf.add_argument('-e','--extra-params',action='append',help='Extra parameter for HLTMPPU, can be repeated multiple times(Example: "dumpThreads=1"')
    puConf.add_argument('-t','--soft-timeout-fraction',type=float,nargs=1,default=0.8,help="Fraction of hard timeout to trigger soft timeout.")
    puConf.add_argument('-T','--hard-timeout',type=int,nargs=1,default=60000,help="Hard timeout duration in milliseconds.")
    puConf.add_argument("-L","--log-name",nargs='?',default="",help="Suffix of child log file name. If empty, it is set to PID-TIMESTAMP")
    puConf.add_argument('--hltresultSizeMb',type=int,default=10,help="Maximum size of HLT result FullEventFragment in MB, defines the size of shared memory segment")

    puConf.add_argument('--debug', '-d', nargs='?', const='child', choices=['parent','child'],
                        help='Attach debugger (to child by default)')
    puConf.add_argument('--scriptAfterFork', help='Execute a command after forking. The command has to be in the form "whichProc:cmd"'
                        ' where whichProc is one of [mother,firstFork,allForks] and cmd can contain a format string {pid} which will be'
                        ' replaced with the PID of the corresponding process (mother or fork).')

    p.add_argument("-I",'--with-infrastructure',action='store_true',help="Whether to start ipc and IS infrastructure")
    p.add_argument("-r","--run-number",type=int,nargs='?',default=0,help="Run number. If empty, it will be read from input file")
    p.add_argument("-l","--log-root",nargs='?',default="/tmp/",help="directory to save log files of child processes")
    p.add_argument("-p","--partition-name",nargs='?',default="",help="Partition Name")
    p.add_argument("--schema-files",action='append',default=[],help="Additonal schema files for RDB server")

    p.add_argument("-O","--options-file",nargs=1,help="Read configuration from options file")
    p.add_argument("-S","--save-options",nargs=1,help="Write configuration to options file. Extension defines the format (json,yaml,xml)")

    p.add_argument("--toroid-current",nargs='?',type=float,default=20400,const=20400,help="Value of the toroid current to pass during prepareForRun")
    p.add_argument("--solenoid-current",nargs='?',type=float,default=7730,const=7730,help="Value of the solenoid current to pass during prepareForRun")
    p.add_argument("--date",nargs='?',default=None,const="0",help="Run start date to be passed during prepareForRun")
    p.add_argument("--detector-mask",nargs='?',default="",help="Detector mask. If empty, it will be read from input file")
    p.add_argument('--ros2robs',default="{}",
                   help="Either a string in the form of python dictionary that contains ros-rob mappings,"
                   " or a file path that contains such string such as /path/to/rosmap.txt or "
                   "{'ROS0':[0x11205,0x11206],'ROS1':[2120005,2120006]}")

    p.add_argument('--cfgdict',metavar='DICT', type=arg_eval, default={},
                   help="Enter a python dictionary to modify any element of the config dictionary. "
                   "This dictionary will be merged with output of getConfigDictionary()")

    commands=sys.argv[1:]
    modGroups={'datasource':['dffileds','dcmds'],
               'monitoring':['monsvcis'],
               'trigger':['joboptions','DBPython','DB','pudummy'],
               'HLTMPPU':['HLTMPPU'],
               }
    modNames=[]
    for k in list(modGroups.keys()):
        modNames.extend(modGroups[k])
    needDefault=True
    modMap={}
    modCount={}
    for k in list(modGroups.keys()):
        modCount[k]=[]
        for m in modGroups[k]:
            modMap[m]=k

    for m in modNames:
        if m in commands:
            needDefault=False
    if needDefault: commands.extend(['HLTMPPU'])

    extra_namespaces=parse_extra(p,commands)
    print("")
    for n in extra_namespaces:
        g=modMap[n.module]
        modCount[g].append(n.module)
        print("Module __'{}'__  {}".format(n.module,n))
        print("")
    for m in list(modCount.keys()):
        if len(modCount[m]) > 1:

            _BigHelp(None,None)(p,None,None,None,'ERROR! More than one module type defined for module class %s %s'%(m,modCount[m]))
        if len(modCount[m]) == 0:
            defMod=modGroups[m][0]
            modCount[m].append(defMod)
            print("Adding default Module '%s' for type '%s' "%(defMod,m))
            extra_namespaces.append((p.parse_known_args(["%s"%((modGroups[m])[0])]))[0])
    print("Final namespaces {}".format(extra_namespaces))
    import pprint
    pp=pprint.PrettyPrinter()
    from os import environ as env
    if extra_namespaces[0].partition_name=="":
        if "TDAQ_PARTITION" not in env:
            extra_namespaces[0].partition_name="HLTMPPy_partition"
        else:
            extra_namespaces[0].partition_name=env['TDAQ_PARTITION']
    runHLTMPPy_dict=getConfigDictionary(extra_namespaces,modMap)
    print("runHLTMPPy dict:")
    pp.pprint(runHLTMPPy_dict)

    from HLTMPPy.runner import runHLTMPPy
    runHLTMPPy(runHLTMPPy_dict)

if "__main__" in __name__:
 main()

