#include <thread>
#include <sstream>

#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

#include "HLTMPPU/DcmEmulator/DFDcmEmuSession.h"
#include "HLTMPPU/DcmEmulator/DFDcmEmuEvent.h"
#include "HLTMPPU/DcmEmulator/SharedMemoryUtils.h"

extern "C" std::unique_ptr<daq::dfinterface::Session> createSession(const boost::property_tree::ptree &conf){
  std::unique_ptr<daq::dfinterface::Session> s(new HLTMP::DFDcmEmuSession(conf));
  return s;
}

using namespace boost::interprocess;

HLTMP::DFDcmEmuSession::DFDcmEmuSession(const boost::property_tree::ptree &cargs):
    m_roshits(cargs.get_child("Configuration.ROS2ROBS")) {

  boost::property_tree::ptree args=cargs.get_child("Configuration.HLTMPPUApplication.DataSource.HLTDFFileBackend");
  // Add each ROBID in extraL1Robs
  auto robit = args.get_child("extraL1Robs").equal_range("ROBID");
  BOOST_FOREACH(const boost::property_tree::ptree::value_type &v, robit) {
    m_extraL1Robs.push_back(v.second.get_value<uint32_t>());
    ERS_DEBUG(1, " Adding Extra L1 ROB " << v.second.get_value<uint32_t>());
  }
}

void HLTMP::DFDcmEmuSession::open(const std::string& name){
  ERS_LOG("Opening Session with Dcm. Client name: " << name);
  m_name = name;
  //Create a shared memory object.
  while (1) {
    try {  // This block will throw an interprocess_exception if the shared memory isn't created yet
      m_shm = std::make_unique<shared_memory_object>(open_only, name.c_str(), read_write);
      m_region = std::make_unique<mapped_region>(*m_shm, read_write);

      //Get the address of the mapped region
      void * addr     = m_region->get_address();

      //Obtain a pointer to the shared structure
      m_session_info = static_cast<session_info*>(addr);
      ERS_LOG(m_session_info->tostring());

      break;
    } catch (interprocess_exception & e) {  // TODO(cyildiz): Wait up to few seconds, then throw
      ERS_DEBUG(5,"Probably shared memory area doesn't exist, wait 100 millisecond for DCM to initialize. "
                  << "Exception : " << e.what());
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    } catch (std::exception & e) {
      std::cout << "Unexpected expection: " << e.what() << std::endl;
      throw daq::dfinterface::CommunicationError(ERS_HERE, " Communication problem?");
    }
  }

}

bool HLTMP::DFDcmEmuSession::isOpen() const{
  if (m_session_info) {
    return true;
  } else {
    return false;
  }

}

void HLTMP::DFDcmEmuSession::close(){
  m_session_info = nullptr;
}


std::unique_ptr<daq::dfinterface::Event> HLTMP::DFDcmEmuSession::getNext() {
  return std::unique_ptr<daq::dfinterface::Event>();
}

std::unique_ptr<daq::dfinterface::Event> HLTMP::DFDcmEmuSession::tryGetNextUntil(const std::chrono::steady_clock::time_point& absTime){
  // Check if dcm already has thrown NoMoreEvents,
  // This would only happen if a fork is restarted at the same time run is stopped
  if (m_session_info->nomoreevents) {
    ERS_LOG("No more events, returning (session: " << m_name << ")");
    throw daq::dfinterface::NoMoreEvents(ERS_HERE,"No more events");
  }

  // Wait until dcm goes into listening state
  while (!m_session_info->dcm_listening) {
    ERS_DEBUG(5,"Wait 5 milliseconds for dcm to go into listening state");
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
    if (m_session_info->nomoreevents) {
      ERS_LOG("No more events, returning (session: " << m_name << ")");
      throw daq::dfinterface::NoMoreEvents(ERS_HERE,"No more events");
    }
  }

  ERS_DEBUG(2,"Locking mutex");  // Wait for DCM to release mutex
  scoped_lock<interprocess_mutex> lock(m_session_info->mutex);
  ERS_DEBUG(2,"Locked mutex");

  m_session_info->cond_next_requested.notify_one();
  ERS_DEBUG(2,"Notified next, Waiting for event to be read in memory");
  auto stopWaiting = [this, &absTime]{
    return m_session_info->nomoreevents || m_session_info->new_event_available || std::chrono::steady_clock::now() > absTime;
  };
  while (!stopWaiting()) {
    ERS_DEBUG(5,"No event yet, check again in 5 ms");
    m_session_info->cond_next_requested.notify_one();
    m_session_info->cond_next_provided.wait_for(lock, std::chrono::milliseconds(5), stopWaiting);
  }

  // DCM may have set nomoreevents=1 and notified all sessions.
  if (m_session_info->nomoreevents) {
    ERS_LOG("No more events, returning (session: " << m_name << ")");
    throw daq::dfinterface::NoMoreEvents(ERS_HERE,"No more events");
  }

  // DCM may have not provided new event temporarily (rate control)
  if (!m_session_info->new_event_available) {
    ERS_LOG("No new event provided within the timeout limit (session: " << m_name << ")");
    throw daq::dfinterface::OperationTimedOut(ERS_HERE,"tryGetNextUntil timed out");
  }

  // Open the full event shared memory
  auto fullevent_handler = SharedVectorHandler(std::string(m_session_info->shared_name) + "_fullevent");
  auto fullev_vec = fullevent_handler.getVectorPtr();
  m_session_info->new_event_available = false; // reset the new event flag

  // Following may happen if there was a problem filling shared vector with full event data
  if (fullev_vec->empty()) {
    auto issue = daq::dfinterface::CommunicationError(ERS_HERE, " Full event vector is empty");
    ers::warning(issue);
    throw issue;
  }
  uint32_t nwords = (*fullev_vec)[1];  // Second word in fragment is number of total words in event
  auto blob = std::make_unique<uint32_t[]>(nwords);

  ERS_DEBUG(4, m_session_info->ev_info.tostring());

  ERS_DEBUG(5, "Copying fullevent from shared memory");
  // Copy word by word into blob
  std::memcpy(blob.get(), fullev_vec->data(), nwords*sizeof(uint32_t));

  // TODO(cyildiz): Can DFDcmEmuEvent use fullev_vec->data() instead of the uniqueptr??? Unnecesarry copying
  auto dffileevent = std::make_unique<HLTMP::DFDcmEmuEvent>(std::move(blob), *this);

  dffileevent->setExtraL1Robs(m_extraL1Robs);

  return dffileevent;
}

std::unique_ptr<daq::dfinterface::Event> HLTMP::DFDcmEmuSession::tryGetNextFor(const std::chrono::steady_clock::duration& /*relTime*/){
  return std::unique_ptr<daq::dfinterface::Event>();
}


void HLTMP::DFDcmEmuSession::accept(std::unique_ptr<daq::dfinterface::Event> event /*event*/,
                                  std::unique_ptr<uint32_t[]> hltr) {
  ERS_DEBUG(3,"Enter accept, session: " << m_session_info->shared_name << ", gid: " << event->gid());

  // Obtain a reference to the shared event structure and mark event as accepted
  event_info& ev_info = m_session_info->ev_info;
  ev_info.accept = true;
  ERS_DEBUG(4, ev_info.tostring());

  // Copy serialized hltresult event into the shared memory
  try {
    auto hltresult_handler = SharedVectorHandler(std::string(m_session_info->shared_name) + "_hltresult");
    uint32_t nwords = hltr.get()[1];  // Second word in fragment is number of total words in event
    hltresult_handler.assign(hltr.get(), nwords);
    ERS_DEBUG(4,"HLTresult: " << hltresult_handler.tostring());
  } catch (const std::exception& e) {
    std::stringstream stream;
    stream << "- Event accepted (gid:" << event->gid() << "), but hlt results can't be copied to shared memory. Rejecting.\n";
    stream << "Original exception: " << e.what();
    auto issue = daq::dfinterface::CommunicationError(ERS_HERE, stream.str());
    ers::warning(issue);
    ev_info.accept = false;
  }

  auto roshitstr = m_roshits.to_json_string();
  std::strcpy(ev_info.ros_stats, roshitstr.c_str());
  m_roshits.clear();

  m_session_info->cond_output_processing_requested.notify_one();  // Ask dcm to process the written hltresult
  ERS_DEBUG(2,"Notified dcm that event is sent");

  // Deadlock protection - in some cases reject/accept notification for cond_output_processing_requested
  // arrives to DcmEmulator::waitForEventDone before it starts listening for it, resulting in a deadlock.
  // Keep sending the notification until waitForEventDone flags the end of event processing
  while (m_session_info->processing_event) {
    ERS_DEBUG(5,"dcm hasn't finished processing event, try notifying dcm again that we sent the event");
    m_session_info->cond_output_processing_requested.notify_one();
    std::this_thread::sleep_for(std::chrono::milliseconds(3));
  }

  // At this point this function can return, as event is already in shared memory
}

void HLTMP::DFDcmEmuSession::reject(std::unique_ptr<daq::dfinterface::Event> event){
  ERS_DEBUG(3,"Enter reject, session: " << m_session_info->shared_name << ", gid: " << event->gid());

  // Obtain a reference to the shared event structure and mark event as rejected
  event_info& ev_info = m_session_info->ev_info;
  ev_info.accept = false;
  ERS_DEBUG(4, ev_info.tostring());

  auto roshitstr = m_roshits.to_json_string();
  std::strcpy(ev_info.ros_stats, roshitstr.c_str());
  m_roshits.clear();

  m_session_info->cond_output_processing_requested.notify_one();  // Ask dcm to process the written hltresult
  ERS_DEBUG(2,"Notified dcm that event is sent");

  // Deadlock protection - in some cases reject/accept notification for cond_output_processing_requested
  // arrives to DcmEmulator::waitForEventDone before it starts listening for it, resulting in a deadlock.
  // Keep sending the notification until waitForEventDone flags the end of event processing
  while (m_session_info->processing_event) {
    ERS_DEBUG(5,"dcm hasn't finished processing event, try notifying dcm again that we sent the event");
    m_session_info->cond_output_processing_requested.notify_one();
    std::this_thread::sleep_for(std::chrono::milliseconds(3));
  }
}

HLTMP::DFDcmEmuSession::~DFDcmEmuSession() {}

HLTMP::DFDcmEmuSession::ROSHits::ROSHits(const boost::property_tree::ptree & ros2robs) {
  using ptree=boost::property_tree::ptree;
  BOOST_FOREACH(const ptree::value_type &rostree, ros2robs) {
    std::string rosname = rostree.first;
    ptree robs = rostree.second;
    BOOST_FOREACH(const ptree::value_type &rob, robs) {
        uint32_t robid = rob.second.get_value<uint32_t>();
        m_rob2ros[robid] = rosname;
    }

    // Initialize map Hits object for each ROS
    m_roshits_getRobs[rosname] = std::move(Hits());
    m_roshits_getAllRobs[rosname] = std::move(Hits());
  }
}

void HLTMP::DFDcmEmuSession::addPrefetchInfo(std::vector<std::pair<uint32_t, double>> robid_size) {
  ERS_DEBUG(3, "Prefetching nrobs: " << robid_size.size());
  std::lock_guard<std::mutex> lck(m_stat_mutex);
  m_roshits.prefetch(robid_size);
}

void HLTMP::DFDcmEmuSession::addRosHitInfo(std::vector<std::pair<uint32_t, double>> robid_size, bool getall) {
  ERS_DEBUG(3, "nrobs: " << robid_size.size() << " getall? : " << getall);
  std::lock_guard<std::mutex> lck(m_stat_mutex);
  m_roshits.add(robid_size, getall);
}

void HLTMP::DFDcmEmuSession::updateRobCacheInfo(const uint32_t & robid, std::set<uint32_t> & robCache) {
  ERS_DEBUG(3, "updateRobCacheInfo for rob id = 0x " << std::hex << robid << std::dec);
  std::lock_guard<std::mutex> lck(m_stat_mutex);
  m_roshits.getROBs2prefetch(robid, robCache);
}

void HLTMP::DFDcmEmuSession::ROSHits::add(std::vector<std::pair<uint32_t, double>> rob_size, bool getall) {
  auto & roshits = (getall) ? m_roshits_getAllRobs : m_roshits_getRobs;

  std::set<std::string> roses_that_got_hit;  // List of ROSes we access in this call
  for (auto & [robid, size] : rob_size) {
    if (m_rob2ros.find(robid) == m_rob2ros.end()) {
      ERS_DEBUG(2, "Unknown ROBID: " << robid);
      continue; // The ROB is not in our ROS2ROB map, can't determine which ROS it comes from
    }
    auto ros = m_rob2ros[robid];

    // Check if the ROB may be already prefetched
    if (m_robs_prefetched.find(ros) != m_robs_prefetched.end()) { // There are ROBs already prefetched from same ROS
      auto & robid_size_map = m_robs_prefetched[ros];
      if (robid_size_map.find(robid) != robid_size_map.end()) {  // ROB is already prefetched
        continue; // Don't increase any statistics, already retrieved
      }
    }

    // Check if the ROS has already been accessed
    if (roses_that_got_hit.find(ros) == roses_that_got_hit.end()) {
      roshits[ros].ros++; // Only add ros hit
      roses_that_got_hit.insert(ros);

      // Check if there are more robs waiting to be fetched from the same ROS
      if (m_robs_tobe_prefetched.find(ros) != m_robs_tobe_prefetched.end()) {
        auto robid_size_map = m_robs_tobe_prefetched[ros];

        // Add statistics of robs that are prefetched
        for (auto & [robid_, size_] : robid_size_map) {
	  if (m_robs_prefetched[ros].find(robid_) == m_robs_prefetched[ros].end()) { // avoid duplicate entries 
	    m_robs_prefetched[ros][robid_] = size_;  // Add rob to prefetched list
	    if (robid != robid_) {  // Don't add if this is the original ROB we look for, it will be added below
	      roshits[ros].rob++;
	      roshits[ros].size = roshits[ros].size + size_;
	    }
	  }
	}

        m_robs_tobe_prefetched.erase(ros);
      }
    }
    roshits[ros].rob++;
    roshits[ros].size = roshits[ros].size + size;
  }
}

void HLTMP::DFDcmEmuSession::ROSHits::prefetch(std::vector<std::pair<uint32_t, double>> rob_size) {
  for (auto & [robid, size] : rob_size) {
    if (m_rob2ros.find(robid) == m_rob2ros.end()) {
      ERS_DEBUG(2, "Unknown ROBID: " << robid);
      continue; // The ROB is not in our ROS2ROB map, can't determine which ROS it comes from
    }
    auto ros = m_rob2ros[robid];
    if (m_robs_tobe_prefetched.find(ros) == m_robs_tobe_prefetched.end()) { // New ROS
      m_robs_tobe_prefetched[ros] = PrefetchMap();
    }
    if (m_robs_tobe_prefetched[ros].find(robid) == m_robs_tobe_prefetched[ros].end()) { // avoid duplicates
      m_robs_tobe_prefetched[ros][robid] = size;
    }
  }
}

std::string HLTMP::DFDcmEmuSession::ROSHits::to_json_string() {
  /*
   *    {
   *      "ROS1": {
   *        "noevbuild": {"ros": 12, "rob": 42, "size": 13123.23"},
   *        "evbuild": {"ros": 12, "rob": 42, "size": 13123.23"}
   *      },
   *      "ROS2": {
   *        "noevbuild": {"ros": 12, "rob": 42, "size": 13123.23"},
   *        "evbuild": {"ros": 12, "rob": 42, "size": 13123.23"}
   *      },
   *      ...
   *    }
   */
  using boost::property_tree::ptree;
  ptree result;
  for (auto & [ros, hits] : m_roshits_getRobs) {
    auto evbuild_hits = m_roshits_getAllRobs[ros];
    ptree rospt;
    ptree noevbuild;
    ptree evbuild;

    std::stringstream ss;
    ss << std::setprecision(4) << hits.size;

    noevbuild.put("ros", hits.ros);
    noevbuild.put("rob", hits.rob);
    noevbuild.put("size", ss.str());

    ss.str(std::string());
    ss << std::setprecision(4) << evbuild_hits.size;

    evbuild.put("ros", evbuild_hits.ros);
    evbuild.put("rob", evbuild_hits.rob);
    evbuild.put("size", ss.str());

    rospt.add_child("evbuild", evbuild);
    rospt.add_child("noevbuild", noevbuild);
    result.add_child(ros,rospt);
  }
  std::stringstream ss;
  bool pretty = false;
  write_json(ss,result,pretty);
  return ss.str();
}

void HLTMP::DFDcmEmuSession::ROSHits::clear() {
  for (auto & [ros,hits] : m_roshits_getRobs) {
    hits = {0,0,0.0};
  }
  for (auto & [ros,hits] : m_roshits_getAllRobs) {
    hits = {0,0,0.0};
  }
  m_robs_prefetched.clear();
  m_robs_tobe_prefetched.clear();
}

void HLTMP::DFDcmEmuSession::ROSHits::getROBs2prefetch(const uint32_t & robid, std::set<uint32_t> & robs2prefetch) {
  if (m_rob2ros.find(robid) == m_rob2ros.end()) {
    ERS_DEBUG(2, "Unknown ROBID: " << robid);
    return; // The ROB is not in our ROS2ROB map, can't determine which ROS it comes from
  }
  auto ros = m_rob2ros[robid];

  // Check if there are more robs waiting to be fetched from the same ROS
  if (m_robs_tobe_prefetched.find(ros) != m_robs_tobe_prefetched.end()) {
    for (auto & [rob, size] : m_robs_tobe_prefetched[ros]) {
      robs2prefetch.insert(rob);
    }
  }
}
