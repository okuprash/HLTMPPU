
#include "HLTMPPU/DcmEmulator/DcmEmulator.h"
#include "HLTMPPU/Issues.h"
#include "HLTMPPU/eformat_utils.h"

#include <cstring>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <chrono>
#include <sstream>
#include <string>
#include <future>
#include <list>

#include <boost/foreach.hpp>
#include "boost/property_tree/json_parser.hpp"
#include <boost/interprocess/exceptions.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/shared_memory_object.hpp>


#include "eformat/eformat.h"
#include "eformat/index.h"
#include "eformat/write/eformat.h"
#include "eformat/SourceIdentifier.h"
#include "eformat/FullEventFragmentNoTemplates.h"

using namespace boost::interprocess;

HLTMP::DcmEmulator::DcmEmulator(const boost::property_tree::ptree & cargs):
    m_ros_hit_stats_accept(cargs), m_ros_hit_stats_reject(cargs) {
  m_nworkers = cargs.get("Configuration.HLTMPPUApplication.numForks", 1);
  m_nevents_per_worker = cargs.get("Configuration.HLTMPPUApplication.numberOfEventSlots", 1);

  // Dcm Emulator is run if we use HLTDFFileBackend and DFDcmEmuBackend as the library
  m_args = cargs.get_child("Configuration.HLTMPPUApplication.DataSource.HLTDFFileBackend");
  m_comp = eformat::UNCOMPRESSED;
  m_compLevel = 0;
  try {
    auto of = m_args.get_child("compressionFormat");
    std::string ctype(of.data());
    if (ctype == "ZLIB") {
      m_comp = eformat::ZLIB;
      m_compLevel = m_args.get("compressionLevel", 2);
    }
  } catch(boost::property_tree::ptree_bad_path &ex) {
    ERS_DEBUG(1, "Compression is not specified");
    ERS_LOG("Failed to get Compression information");
  }

  m_ros_hit_stats_basename = m_args.get("rosHitStatFileName", "ros_hitstats");

  size_t max_fullev_size_MB = m_args.get("maxInputFullEventSizeMb", 2); // Default value is 2MB
  m_max_fullev_size = max_fullev_size_MB * 1024 * 1024;

  size_t max_hltres_size_MB = cargs.get("Configuration.HLTMPPUApplication.maximumHltResultMb", 10);  // Default value is 10MB
  m_max_hltres_size = max_hltres_size_MB * 1024 * 1024;

  float max_event_rate_Hz = m_args.get("maxEventRateHz", -1.0); // <=0 means no limit
  m_min_getNext_interval_ms = max_event_rate_Hz <= 0 ? 0 : static_cast<size_t>(1000.0/max_event_rate_Hz);
  m_min_getNext_time = std::chrono::steady_clock::now();

  // Set base name as the TDAQ_APPLICATION_NAME, as this will be called in runHLTMPPy
  char * base_name = getenv("TDAQ_APPLICATION_NAME");
  if (base_name==nullptr) {
    m_base_name="DummyApplication";
    ERS_LOG("TDAQ_APPLICATION_NAME not in environment, setting a dummy name: " << m_base_name);
  } else {
    m_base_name = std::string(base_name);
    ERS_LOG("Setting name using TDAQ_APPLICATION_NAME: " << m_base_name);
  }
  m_nomoreevents = false;
  m_readEvents = 0;
  m_stop = false;

  // Clean up shared memory from previous sessions, in case not cleaned!
  ERS_DEBUG(1, "Removing any dangling shared memory with same name");
  for (size_t i = 1; i < m_nworkers+1 ; i++) {
    for (size_t j = 0; j < m_nevents_per_worker ; j++) {
      std::stringstream ss;
      ss << m_base_name << "-" << std::setfill('0') << std::setw(2) << i << "-" << std::setfill('0') << std::setw(2) << j;
      auto name = ss.str();
      shared_memory_object::remove(name.c_str());
    }
  }
}

HLTMP::DcmEmulator::~DcmEmulator() {
  ERS_DEBUG(1, "Destroying DcmEmulator");
}

std::unique_ptr<uint32_t[]> HLTMP::DcmEmulator::getNextEvent() {
  std::unique_ptr<uint32_t[]> blob;

  // Lock event getter, multiple workers can be asking for events at the same time
  ERS_DEBUG(2, "Locking read_mutex");
  std::lock_guard<std::mutex> lock_next(m_readmutex);
  ERS_DEBUG(2, "Locked read_mutex");

  // Return immediately if no more events or stopRun is called
  if (m_nomoreevents || m_stop) {
    return blob;
  }

  // Check if input rate control allows us to read new event
  auto now = std::chrono::steady_clock::now();
  if (m_min_getNext_interval_ms > 0 && now < m_min_getNext_time) {
    return blob;
  }
  m_min_getNext_time = now + std::chrono::milliseconds(m_min_getNext_interval_ms);

  try {
    blob = m_file_rw->getNextEvent();
    m_readEvents++;
    ERS_DEBUG(2, "Event number: " << m_readEvents);
  } catch (HLTMP::NoMoreEventsInFile & ex) {
    ERS_LOG("No more events!");
    m_nomoreevents = 1;
    for (auto& [session_name, handler] : m_sessions) {
      handler.m_session_info->nomoreevents = true;
    }
  }
  return blob;
}

void HLTMP::DcmEmulator::configure() {
}

void HLTMP::DcmEmulator::connect() {
}

void HLTMP::DcmEmulator::prepareForRun(const boost::property_tree::ptree & pargs) {
  ERS_DEBUG(1, "Enter");

  // Create a handler for each session
  // ApplicationName-XX-YY  (XX: worker starting from 1, YY: session starting from 0)
  for (size_t i = 1; i < m_nworkers+1 ; i++) {
    for (size_t j = 0; j < m_nevents_per_worker ; j++) {
      std::stringstream ss;
      ss << m_base_name << "-" << std::setfill('0') << std::setw(2) << i << "-" << std::setfill('0') << std::setw(2) << j;
      auto name = ss.str();
      m_sessions.try_emplace(name, name, m_max_fullev_size, m_max_hltres_size);
    }
  }

  m_stop = false;
  m_nomoreevents = false;

  // Initialize file reader writer
  m_args.add_child("RunParams", pargs.get_child("RunParams"));
  m_file_rw = std::make_unique<FileReaderWriter>(m_args);

  for (auto &[session_name, handler] : m_sessions) {
    m_event_futures[session_name];  // Create an empty list for each session
    m_session_threads.emplace_back(&DcmEmulator::listenSession, this, session_name);
  }

  ERS_DEBUG(1, "Exit");
}

void HLTMP::DcmEmulator::stopRun() {
  ERS_DEBUG(1, "Enter");
  m_stop = true;

  ERS_LOG("Notifying all session threads that are waiting for eventDone calls");
  for (auto &[session_name, handler] : m_sessions) {
    handler.m_session_info->cond_output_processing_requested.notify_all();
  }

  {
    std::lock_guard<std::mutex> lock_next(m_eventfuture_mutex);
    ERS_LOG("Waiting for " << m_event_futures.size() << " event threads to join");
    for (auto & [sname,list] : m_event_futures) {
      for (auto & future : list) {
        future.wait();
      }
    }
    m_event_futures.clear();
  }

  // Notify session threads that are waiting for event requests
  ERS_LOG("Notifying all session threads that are waiting in getNext for new event data");
  for (auto &[session_name, handler] : m_sessions) {
    handler.m_session_info->cond_next_provided.notify_all();
  }
  ERS_LOG("Notifying all session threads that are waiting for getNext calls");
  for (auto &[session_name, handler] : m_sessions) {
    handler.m_session_info->cond_next_requested.notify_all();
  }
  ERS_LOG("Notifying all session threads that are waiting for finished events");
  for (auto &[session_name, handler] : m_sessions) {
    handler.m_session_info->cond_output_processing_finished.notify_all();
  }
  ERS_LOG("Waiting for " << m_session_threads.size() << " session threads to join");
  for (auto && thread : m_session_threads) {
    thread.join();
  }
  ERS_LOG("Events processed: " << m_readEvents);
  // Delete all DcmSessionHandler objects
  m_sessions.clear();
  m_session_threads.clear();

  // Delete file reader, this will finish writing and close the file
  m_file_rw.reset();

  // Following part won't do anything, if ROS2ROB map is empty
  if (m_ros_hit_stats_reject.isEnabled() || m_ros_hit_stats_accept.isEnabled()) {
    m_ros_hit_stats_reject.calculate();
    m_ros_hit_stats_accept.calculate();

    std::string filename = m_ros_hit_stats_basename + "_reject.txt";
    std::ofstream ostrm_r(filename);
    m_ros_hit_stats_reject.print(ostrm_r);

    filename = m_ros_hit_stats_basename + "_accept.txt";
    std::ofstream ostrm_a(filename);
    m_ros_hit_stats_accept.print(ostrm_a);
  }
  ERS_DEBUG(1, "Exit");
}

void HLTMP::DcmEmulator::disconnect() {
}

void HLTMP::DcmEmulator::unconfigure() {
}

void HLTMP::DcmEmulator::cleanup_futures(std::string session_name) {
  std::lock_guard<std::mutex> lock_next(m_eventfuture_mutex);
  std::future_status status;
  auto & list = m_event_futures[session_name];
  ERS_DEBUG(3, "Session: " << session_name << ", Number of futures: " << list.size());
  for (auto it = list.begin(); it != list.end();) {
    status = it->wait_for(std::chrono::microseconds(1));
    if (status == std::future_status::ready) { // This means function call completed and we can remove it
      ERS_DEBUG(5, "Future ready, erasing from list");
      it = list.erase(it);  // Automatically does it++
    } else if (status == std::future_status::timeout) {
      ERS_DEBUG(5, "Future timeout, future not ready,");
      it++;
    } else if (status == std::future_status::deferred) {
      ERS_DEBUG(5, "Future deferred, this should not have happened!");
      it++;
    }
  }
}

void HLTMP::DcmEmulator::listenSession(std::string session_name) {
  ERS_DEBUG(1, "Enter listenSession for " << session_name);
  session_info* data = m_sessions.at(session_name).m_session_info;

  ERS_DEBUG(2, session_name << " listenSession trying to lock mutex");
  scoped_lock<interprocess_mutex> lock(data->mutex);
  ERS_DEBUG(2, session_name << " listenSession locked mutex");

  do {
    // Wait for getNext requests (releases lock while waiting, then re-acquires)
    ERS_DEBUG(2, session_name << " listenSession waiting for next event request");
    data->dcm_listening = true;
    data->cond_next_requested.wait(lock);
    data->dcm_listening = false;
    ERS_DEBUG(2, session_name << " listenSession received next event request");

    // Read event into memory
    std::unique_ptr<uint32_t[]> blob = getNextEvent();

    if (m_nomoreevents || m_stop) {
      data->nomoreevents = true;
      data->cond_next_provided.notify_one();
      continue;
    }

    const bool eventSkipped{blob==nullptr};

    // Write new event data to shared memory
    m_sessions.at(session_name).newEvent(std::move(blob));
    ERS_DEBUG(2, session_name << " New event written to shared memory");

    cleanup_futures(session_name);

    if (!eventSkipped) {
      // Flag the session as processing an event
      data->processing_event = true;
      {
        std::lock_guard<std::mutex> lock_next(m_eventfuture_mutex);

        // Start waitForEventDone asyncronously, put it's future in the container for cleaup later
        // std::async is preferred instead of std::thread. Because the future can indicate if the function exited
        // and this helps periodical cleanup
        m_event_futures[session_name].push_back(std::async(std::launch::async, &DcmEmulator::waitForEventDone, this, session_name));
      }
    }

    // Notify the process that the event is ready
    data->cond_next_provided.notify_one();

    // Wait until this event processing is finished (releases lock while waiting, then re-acquires)
    ERS_DEBUG(2, session_name << " listenSession waiting for end of event processing");
    if (data->processing_event) { // protect in case processing finished before we arrived here (wait below would deadlock)
      data->cond_output_processing_finished.wait(lock);
    } else {
      cleanup_futures(session_name);
    }
    ERS_DEBUG(2, session_name << " listenSession finished waiting for end of event processing");
  } while (!m_nomoreevents && !m_stop);

  ERS_DEBUG(1, session_name << " listenSession exit");
}

void HLTMP::DcmEmulator::waitForEventDone(const std::string& session_name) {
  ERS_DEBUG(2, "Enter waitForEventDone for " << session_name);
  session_info* session = m_sessions.at(session_name).m_session_info;
  const event_info& ev_info = session->ev_info;

  ERS_DEBUG(3, session_name << " waitForEventDone trying to lock mutex");
  scoped_lock<interprocess_mutex> lock(session->mutex);
  ERS_DEBUG(3, session_name << " waitForEventDone locked mutex");

  // What happens if worker crashes during processing?
  // Normally HLTSV would notify DCM?
  // TODO(cyildiz): Find a way to make Dcm aware if a session disconnected
  ERS_DEBUG(3, session_name << " waitForEventDone waiting for HLT result");
  session->cond_output_processing_requested.wait(lock);
  ERS_DEBUG(3, session_name << " waitForEventDone finished waiting for HLT result");

  if (ev_info.accept) {
    const uint32_t* fulleventptr = session->fullev_shared_vec->getVectorPtr()->data();
    const uint32_t* hltresultptr = session->hltres_shared_vec->getVectorPtr()->data();

    ERS_DEBUG(2, session_name << " Accepting event");
    std::vector<uint32_t> finalEvent;
    finalEvent = merge_hltresult_with_input_event(hltresultptr, fulleventptr, m_comp, m_compLevel);

    auto finalSize = finalEvent.size();
    auto sizeInBytes = finalSize*4;  // finalSize is in words(4 byte chunks), putData argument is bytes
    ERS_DEBUG(2, "Writing " << sizeInBytes << " bytes ");
    {
      std::lock_guard<std::mutex> lock_next(m_writemutex);  // Protect consecutive write actions
      m_file_rw->writeEvent(sizeInBytes, finalEvent.data());
    }
    std::lock_guard<std::mutex> lock(m_statmutex);  // Protect updating of ros stats
    m_ros_hit_stats_accept.add(ev_info.ros_stats);
  }  else {
    ERS_DEBUG(2, session_name << " Rejecting event");
    std::lock_guard<std::mutex> lock(m_statmutex);  // Protect updating of ros stats
    m_ros_hit_stats_reject.add(ev_info.ros_stats);
  }
  ERS_DEBUG(4, "ROS Stats for " << session_name << " : " << ev_info.ros_stats);

  // Notify the getNext thread that new event can be inserted for this session
  session->processing_event = false; // Flag the session finished processing an event
  session->cond_output_processing_finished.notify_one();

  ERS_DEBUG(2, session_name << " waitForEventDone exit");
}

HLTMP::DcmSessionHandler::DcmSessionHandler(const std::string name, size_t fullev_shm_size, size_t hltres_shm_size): m_name(name) {
  ERS_LOG("DCM session handler for: " << name);

  m_smh = std::make_unique<SharedMemoryHandler>(name, sizeof(session_info));

  m_session_info = new (m_smh->getAddress()) session_info;
  std::strncpy(m_session_info->shared_name, name.c_str(), sizeof(m_session_info->shared_name)-1);
  m_session_info->fullev_shared_vec = std::make_unique<SharedVectorHandler>(name + "_fullevent", fullev_shm_size);
  m_session_info->hltres_shared_vec = std::make_unique<SharedVectorHandler>(name + "_hltresult", hltres_shm_size);
}

void HLTMP::DcmSessionHandler::newEvent(std::unique_ptr<uint32_t[]> blob) {
  // Reset event_info
  m_session_info->ev_info = event_info{};

  // Rate control skipping the new event request
  if (blob==nullptr) {
    m_session_info->new_event_available = false;
    return;
  }

  // Get global_id of event
  eformat::read::FullEventFragment event(blob.get());
  m_session_info->ev_info.globalID = event.global_id();

  ERS_DEBUG(2, m_session_info->shared_name << " received new event with gid " << m_session_info->ev_info.globalID);

  auto ptr = blob.get();
  uint32_t nwords = event.fragment_size_word();  // Size of FullEventFragment in words

  ERS_DEBUG(4, "Before copying: \n"
            << "nFirst 3: " << std::hex << ptr[0] << "\n"
            << "         "  << std::hex << ptr[1] << "\n"
            << "         "  << std::hex << ptr[2] << "\n"
            << "size:    "  << std::dec << nwords << "\n"
            << "Last 3:  "  << std::hex << ptr[nwords-3] << "\n"
            << "         "  << std::hex << ptr[nwords-2] << "\n"
            << "         "  << std::hex << ptr[nwords-1] << "\n"
            << std::dec << "\n");

  // Copy event into shared vector, after this blob will be destroyed
  m_session_info->fullev_shared_vec->assign(blob.get(), nwords);
  m_session_info->new_event_available = true;
  ERS_DEBUG(4, "after copying: \n" << m_session_info->fullev_shared_vec->tostring());
}

HLTMP::SharedMemoryHandler::SharedMemoryHandler(const std::string name, const size_t mem_size):m_name(name) {
  // Erase previous shared memory with same name, if exists
  ERS_DEBUG(4, "Erasing previous shared memory: " << m_name);
  shared_memory_object::remove(m_name.c_str());

  ERS_DEBUG(3, "Creating shared memory: " << m_name << ", size(bytes): " << mem_size);

  try {
    m_shared_memory = shared_memory_object(create_only, m_name.c_str(), read_write);
  }
  catch (const boost::interprocess::interprocess_exception& ex) {
    throw HLTMPPUIssue::UnexpectedIssue(ERS_HERE, ("Cannot create shared memory segment with name " +
                                                   m_name + ", size " + std::to_string(mem_size)).c_str());
  }

  m_shared_memory.truncate(mem_size);

  m_region = mapped_region(m_shared_memory, read_write);
}

HLTMP::SharedMemoryHandler::~SharedMemoryHandler() {
  ERS_DEBUG(3, "Destroying SharedMemoryHandler instance: " << m_name);
  shared_memory_object::remove(m_name.c_str());
}

HLTMP::ROSHitStats::ROSHitStats(const boost::property_tree::ptree & config) {
  auto ros2robtree = config.get_child("Configuration.ROS2ROBS");
  using ptree=boost::property_tree::ptree;
  BOOST_FOREACH(const ptree::value_type &rostree, ros2robtree) {
    std::string rosname = rostree.first;
    ptree robs = rostree.second;
    size_t nrobs = 0;
    BOOST_FOREACH([[maybe_unused]]const ptree::value_type &rob, robs) {
        nrobs++;
    }
    m_nrobs[rosname] = nrobs;

    // Initialize map Hits object for each ROS
    m_total[rosname] = std::move(ROSFractions());
    m_evbuild[rosname] = std::move(ROSFractions());
    m_noevbuild[rosname] = std::move(ROSFractions());
  }
  m_nevents = 0;
  if (m_nrobs.empty()) {
    m_enabled = false;
  } else {
    m_enabled = true;
  }
}

void HLTMP::ROSHitStats::add(std::string event_stats) {
  if (!isEnabled()) {
    return;
  }
  std::stringstream stream;
  stream << event_stats;
  boost::property_tree::ptree stats;
  try {
    boost::property_tree::read_json(stream, stats);

    // Add number of ros hits/rob hits and total size.
    // Normalization with number of robs and events will
    // be done at the end
    for (auto & [ros,nrobs] : m_nrobs) {
      m_evbuild[ros].ros = m_evbuild[ros].ros + stats.get<double>(ros + ".evbuild.ros");
      m_evbuild[ros].rob = m_evbuild[ros].rob + stats.get<double>(ros + ".evbuild.rob");
      m_evbuild[ros].size = m_evbuild[ros].size + stats.get<double>(ros + ".evbuild.size");
      m_noevbuild[ros].ros = m_noevbuild[ros].ros + stats.get<double>(ros + ".noevbuild.ros");
      m_noevbuild[ros].rob = m_noevbuild[ros].rob + stats.get<double>(ros + ".noevbuild.rob");
      m_noevbuild[ros].size = m_noevbuild[ros].size + stats.get<double>(ros + ".noevbuild.size");
    }
    m_nevents++;
  } catch (boost::property_tree::json_parser::json_parser_error & ex) {
    ERS_DEBUG(2, "Problem reading json string due to: " << ex.what()
                 << "\nString content: " << event_stats);
  } catch (boost::property_tree::ptree_bad_path & ex) {
    ERS_DEBUG(2, "Problem getting one or more elements from json string due to: " << ex.what()
                 << "\nString content: " << event_stats);
  }
}

void HLTMP::ROSHitStats::calculate() {
  if (!isEnabled()) {
    return;
  }
  if (!m_nevents) {  // Nothing to calculate
    return;
  }
  for (auto & [ros,nrobs] : m_nrobs) {
    auto & fraceb = m_evbuild[ros];
    auto & fracnoeb = m_noevbuild[ros];
    auto & fractot = m_total[ros];

    auto normalize = [](ROSFractions & frac, size_t m_nevents, size_t nrobs) {
      if ( frac.ros > 0) {
       frac.rob = frac.rob/frac.ros/nrobs;    
       frac.size = frac.size/frac.ros;
      }
      frac.ros = frac.ros/m_nevents; 
    };

    normalize(fraceb,m_nevents,nrobs);
    normalize(fracnoeb,m_nevents,nrobs);

    fractot.ros = fraceb.ros + fracnoeb.ros;
    fractot.rob = fraceb.rob + fracnoeb.rob;
    fractot.size = fraceb.size + fracnoeb.size;
  }
}

void HLTMP::ROSHitStats::print(std::ostream & out) {
  if (!isEnabled()) {
    return;
  }
  out << "Number of Events: " << m_nevents << "\n";
  out << "                            | total                               | no evt. bld.                        | evt. bld.                           |" << "\n"
      << "ROS                | # ROBs | Hits/Evt    ROB frac/Evt  bytes/Evt | Hits/Evt    ROB frac/Evt  bytes/Evt | Hits/Evt    ROB frac/Evt  bytes/Evt |" << "\n";
  for (auto & [ros,nrobs] : m_nrobs) {
    auto & fraceb = m_evbuild[ros];
    auto & fracnoeb = m_noevbuild[ros];
    auto & fractot = m_total[ros];

    auto printone = [](ROSFractions & frac) {
      std::stringstream ss;
      ss << std::setprecision(4) << std::fixed
         << std::setw(9) << frac.ros  << " , "
         << std::setw(9) << frac.rob  << " , "
         << std::setw(11) << frac.size  << " | ";
      return ss.str();
    };

    out << std::setw(18) << ros << " | " << std::setw(6) << nrobs << " | "
              << printone(fractot) << printone(fracnoeb) << printone(fraceb) << "\n";
  }
}
