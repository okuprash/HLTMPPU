// This application emulates dcm
// It is used together with DFFileBackend

#include "HLTMPPU/DcmEmulator/DcmEmulator.h"
#include "HLTMPPU/HLTMPPU_utils.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/program_options.hpp>

#include <chrono>

namespace po = boost::program_options;

int main(int argc,const char *argv[])
{

  std::string config_filename;
  std::string prepareForRun_filename;
  po::options_description desc("This program is an emulator for DCM, used for testing dfinterface");
  desc.add_options()
    ("config-tree,c", po::value<std::string>(&config_filename)->default_value(""),
                  "Name of the xml file with Dcm configuration")
    ("prepareForRun-tree,p", po::value<std::string>(&prepareForRun_filename)->default_value(""),
                  "Name of the xml file with PrepareForRun configuration")
    ("help,h", "Print help message")
    ;

  std::cout<<"Input parameters "<<std::endl;
  for(int i=0;i<argc;i++){
    std::cout<<" "<<i<<" \""<<argv[i]<<"\""<<std::endl;
  }
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
  po::notify(vm);
  if(vm.count("help")) {
    std::cout << desc << std::endl;
    return EXIT_SUCCESS;
  }
  if(config_filename.length()==0 || prepareForRun_filename.length()==0) {
    std::cout << "Empty Filename" << std::endl;
    return EXIT_FAILURE;
  }

  auto config_pt = HLTMP::getTreefromFile(config_filename);
  auto prep_pt = HLTMP::getTreefromFile(prepareForRun_filename);

  HLTMP::DcmEmulator dcm(config_pt);

  dcm.configure();
  dcm.connect();
  dcm.prepareForRun(prep_pt);

  std::cout << "Waiting until there are no more events" << std::endl;
  while (!dcm.noMoreEvents()) {
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
  dcm.stopRun();
  dcm.disconnect();
  dcm.unconfigure();

  return 0;
}

/* Example ptree

<Configuration>
  </ROS2ROBS>
  <HLTMPPUApplication>
    <ApplicationName>App_name_Test</ApplicationName>
    <numForks>2</numForks>
    <numberOfEventSlots>3</numberOfEventSlots>
    <DataSource>
      <HLTDFDcmEmuBackend>
        <numEvents>10</numEvents>
        <start_id>1</start_id>
        <stride>1</stride>
        <library>DFDcmEmuBackend</library>
        <fileOffset>-1</fileOffset>
        <compressionLevel>0</compressionLevel>
        <skipEvents>5</skipEvents>
        <compressionFormat>ZLIB</compressionFormat>
        <outputFileName>test.out</outputFileName>
        <loopOverFiles>False</loopOverFiles>
        <fileList>
          <file>/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/TrigP1Test/data18_13TeV.00364485.physics_EnhancedBias.merge.RAW._lb0705._SFO-1._0001.1</file>
        </fileList>
        <extraL1Robs/>
      </HLTDFDcmEmuBackend>
    </DataSource>
  </HLTMPPUApplication>
</Configuration>

<Configuration> <HLTMPPUApplication> <ApplicationName>App_name_Test</ApplicationName> <numForks>2</numForks> <numberOfEventSlots>3</numberOfEventSlots> <DataSource> <HLTDFFileBackend> <numEvents>10</numEvents> <start_id>1</start_id> <stride>1</stride> <library>DFDcmEmuBackend</library> <fileOffset>-1</fileOffset> <compressionLevel>0</compressionLevel> <skipEvents>5</skipEvents> <compressionFormat>ZLIB</compressionFormat> <outputFileName>test.out</outputFileName> <loopOverFiles>False</loopOverFiles> <fileList> <file>/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/TrigP1Test/data18_13TeV.00364485.physics_EnhancedBias.merge.RAW._lb0705._SFO-1._0001.1</file> </fileList> <extraL1Robs/> </HLTDFFileBackend> </DataSource> </HLTMPPUApplication> </Configuration>

*/
