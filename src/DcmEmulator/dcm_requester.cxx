// Application used for prototyping what goes into DFDcmEmuBackend
// This application mimics calls from a worker

#include <boost/program_options.hpp>

#include <boost/property_tree/ptree.hpp>

#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <sstream>

#include "HLTMPPU/DcmEmulator/DFDcmEmuSession.h"
#include "HLTMPPU/DcmEmulator/DFDcmEmuEvent.h"
#include "HLTMPPU/HLTMPPU_utils.h"

#include "ers/ers.h"
#include "eformat/write/eformat.h"
#include "eformat/FullEventFragmentNoTemplates.h"

std::map<uint64_t, std::unique_ptr<daq::dfinterface::Event>> events;  // Map such as: <global_id, event>
std::unique_ptr<HLTMP::DFDcmEmuSession> session;

using namespace boost::interprocess;

namespace po = boost::program_options;

//! Dump header information of a FullEventFragment (can be read or write)
template<class T>
void dumpEventInfo(T& fullev) {
  ERS_DEBUG(2, "nstatus            : " << fullev.nstatus() << "\n"
            << "bcid               : " << fullev.bc_id() << "\n"
            << "global_id          : " << fullev.global_id() << "\n"
            << "lvl1_id            : " << fullev.lvl1_id() << "\n"
            << "nlvl1_trigger_info : " << fullev.nlvl1_trigger_info() << "\n"
            << "nlvl2_trigger_info : " << fullev.nlvl2_trigger_info() << "\n"
            << "nhlt_info          : " << fullev.nhlt_info() << "\n"
            << "nstream_tag        : " << fullev.nstream_tag() << "\n");
  if (fullev.nlvl1_trigger_info()) {
    ERS_DEBUG(2, "lvl1_trigger_info[0] : " << fullev.lvl1_trigger_info()[0] << "\n");
  }
  if (fullev.nlvl2_trigger_info()) {
    ERS_DEBUG(2, "lvl2_trigger_info[0] : " << fullev.lvl2_trigger_info()[0] << "\n");
  }
  if (fullev.nhlt_info()) {
    ERS_DEBUG(2, "hlt_info[0]          : " << fullev.hlt_info()[0] << "\n");
  }
  if (fullev.nstream_tag()) {
    ERS_DEBUG(2, "stream_tag[0]        : " << fullev.stream_tag()[0] << "\n");
  }
}

void process(uint64_t global_id) {

  // This part mimics athena processing and addition of HLT results, hlt_info and stream tags
  uint32_t hltrob0[] = {0xdd1234dd, 0x0000002e, 0x00000008, 0x05000000, 0x007c0000, 0x00000001, 0x00000000, 0x00000000, 0xee1234ee, 0x00000009, 0x03010000, 0x007c0000, 0x0004fe61, 0xfa001e75, 0x000004d1, 0x00000081, 0x00600065, 0x00000000, 0x00000003, 0x0b2e6b6a, 0x00000001, 0x00000000, 0x00000000, 0x00000030, 0x00000002, 0x00000000, 0x00000000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000008, 0x00000001, 0x4d544c48, 0x5f795050, 0x65687461, 0x4c48616e, 0x31302d54, 0x0a0a0a0a, 0x00000001, 0x00000001, 0x00000019, 0x00000000 };
  uint32_t hltrob1[] = {0xdd1234dd, 0x00000026, 0x00000008, 0x05000000, 0x007c0005, 0x00000001, 0x00000000, 0x00000000, 0xee1234ee, 0x00000009, 0x03010000, 0x007c0005, 0x0004fe61, 0xfa001e75, 0x000004d1, 0x00000081, 0x00600065, 0x00000000, 0x00000003, 0x0b2e6b6a, 0x00000001, 0x00000000, 0x00000000, 0x00000030, 0x00000002, 0x00000000, 0x00000000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000001, 0x00000011, 0x00000000 };
  uint32_t hltrob2[] = {0xdd1234dd, 0x00000026, 0x00000008, 0x05000000, 0x007c000d, 0x00000001, 0x00000000, 0x00000000, 0xee1234ee, 0x00000009, 0x03010000, 0x007c000d, 0x0004fe61, 0xfa001e75, 0x000004d1, 0x00000081, 0x00600065, 0x00000000, 0x00000003, 0x0b2e6b6a, 0x00000001, 0x00000000, 0x00000000, 0x00000030, 0x00000002, 0x00000000, 0x00000000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000001, 0x00000011, 0x00000000 };

  uint32_t stream_tags[] = { 0x656d616e, 0x50794d3d, 0x69737968, 0x72745363, 0x316d6165, 0x7079743b, 0x68703d65, 0x63697379, 0x756c3b73, 0x313d696d, 0x6d616e00, 0x794d3d65, 0x75626544, 0x72745367, 0x316d6165, 0x7079743b, 0x65643d65, 0x3b677562, 0x696d756c, 0x6e00303d, 0x3d656d61, 0x6143794d, 0x5362696c, 0x61657274, 0x743b316d, 0x3d657079, 0x696c6163, 0x74617262, 0x3b6e6f69, 0x696d756c, 0x723b303d, 0x3d73626f, 0x32343030, 0x61323030, 0x32343030, 0x62323030, 0x7465643b, 0x30303d73, 0x30303030, 0x30303134, 0x30303030, 0x6e003234, 0x3d656d61, 0x61746144, 0x756f6353, 0x676e6974, 0x5f35305f, 0x6e6f754d, 0x7079743b, 0x61633d65, 0x7262696c, 0x6f697461, 0x756c3b6e, 0x303d696d, 0x626f723b, 0x30303d73, 0x30306337, 0x6e003530, 0x3d656d61, 0x61746144, 0x756f6353, 0x676e6974, 0x5f33315f, 0x3b74654a, 0x65707974, 0x6c61633d, 0x61726269, 0x6e6f6974, 0x6d756c3b, 0x3b313d69, 0x73626f72, 0x3730303d, 0x30303063, 0x00000064};
  uint32_t hlt_info[] = { 0x0000001e, 0x00800000};

  // Following part is done by Athena,
  // Create a new event, we want to fill only necessary fields.
  eformat::write::FullEventFragment fullevent_hltresult;

  // Add global id
  fullevent_hltresult.global_id(global_id);

  // Add stream tags
  fullevent_hltresult.stream_tag(74, stream_tags);
  fullevent_hltresult.hlt_info(2, hlt_info);

  ERS_DEBUG(2, "--- After new stream tags ---\n"
   << "nstreamtag: " << fullevent_hltresult.nstream_tag() << "\n"
   << "streamtag[0]: " << fullevent_hltresult.stream_tag()[0]);

  const char* multistring = reinterpret_cast<const char*>(fullevent_hltresult.stream_tag());
  std::cout << multistring << std::endl;

  // Add some hlt result robs
  eformat::write::ROBFragment rob0(hltrob0);
  eformat::write::ROBFragment rob1(hltrob1);
  eformat::write::ROBFragment rob2(hltrob2);
  fullevent_hltresult.append(&rob0);
  fullevent_hltresult.append(&rob1);
  fullevent_hltresult.append(&rob2);

  const eformat::write::node_t* top = fullevent_hltresult.bind();
  auto finalSize = fullevent_hltresult.size_word();
  auto finalEvent = std::make_unique<uint32_t[]>(finalSize);
  auto res = eformat::write::copy(*top, finalEvent.get(), finalSize);

  if (res != finalSize) {
    ERS_LOG("ERROR Event serialization failed. Skipping");
    return;
  }

  // Mimic processing duration
  std::this_thread::sleep_for(std::chrono::milliseconds(20));

  // Following is done in DFDataSource::eventDone()
  std::cout << "Accepting" << std::endl;
  session->accept(std::move(events[global_id]), std::move(finalEvent));
  // session.reject(std::move(events[global_id]));

  events.erase(global_id);
  ERS_LOG("Processing done, HLTResults sent");
}

int main(int argc, const char *argv[])
{
  std::string config_filename;
  std::string session_name;
  po::options_description desc("This program is an emulator for DCM, used for testing dfinterface");
  desc.add_options()
    ("filename,f", po::value<std::string>(&config_filename)->default_value(""),
                  "Name of the xml file with Dcm configuration")
    ("sessionname,s", po::value<std::string>(&session_name)->default_value(""),
                  "Name of the session, ")
    ("help,h", "Print help message")
    ;

  std::cout<<"Input parameters "<<std::endl;
  for(int i=0;i<argc;i++){
    std::cout<<" "<<i<<" \""<<argv[i]<<"\""<<std::endl;
  }
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
  po::notify(vm);
  if(vm.count("help")) {
    std::cout << desc << std::endl;
    return EXIT_SUCCESS;
  }
  if(config_filename.length()==0) {
    std::cout << "Empty Filename" << std::endl;
    return EXIT_FAILURE;
  }
  if(session_name.length()==0) {
    std::cout << "Empty session name" << std::endl;
    return EXIT_FAILURE;
  }

  auto pt = HLTMP::getTreefromFile(config_filename);
  session = std::make_unique<HLTMP::DFDcmEmuSession>(pt);

  session->open(session_name);

  size_t nev=0;

  std::vector<std::thread> process_threads;

  while (true) {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::unique_ptr<daq::dfinterface::Event> ev;

    // getNext() and getL1Result are normally done in "DataSource::getNext(l1r)
    try {
      ev = session->getNext();
    } catch (daq::dfinterface::NoMoreEvents &ex) {
      ERS_LOG("NoMoreEvents, returning");
      break;
    } catch (std::exception &ex) {
      ERS_LOG("Caught exception: \"" << ex.what() << "\" returning");
      break;
    }
    nev++;
    std::cout << "Read event " << nev << std::endl;

    std::unique_ptr<uint32_t[]> fullev_read;
    ev->getL1Result(fullev_read);
    eformat::read::FullEventFragment fullevent(fullev_read.get());

    std::cout << "Dumping info after getL1Result" << std::endl;
    dumpEventInfo(fullevent);

    // Print information of first 2 ROB fragments
    eformat::ROBFragment l1r_robf(fullevent.child(0));
    ERS_DEBUG(2, "Got the first child: " << &l1r_robf);
    ERS_DEBUG(2, "Source ID: " << l1r_robf.source_id());
    ERS_DEBUG(2, "ROB first child status size : " << l1r_robf.nstatus());
    ERS_DEBUG(2, "ROB first child rob size : " << l1r_robf.rod_ndata());
    ERS_DEBUG(2, "ROB first child rob[0], rob[N]: " << l1r_robf.rod_data()[0]  << ", " << l1r_robf.rod_data()[l1r_robf.rod_ndata()-1]);

    eformat::ROBFragment l1r_robf2(fullevent.child(1));
    ERS_DEBUG(2, "Got the second child: " << &l1r_robf2);
    ERS_DEBUG(2, "Source ID: " << l1r_robf2.source_id());
    ERS_DEBUG(2, "ROB second child status size : " << l1r_robf2.nstatus());
    ERS_DEBUG(2, "ROB second child rob size : " << l1r_robf2.rod_ndata());
    ERS_DEBUG(2, "ROB second child rob[0], rob0[N]: " << l1r_robf2.rod_data()[0] << ", " << l1r_robf2.rod_data()[l1r_robf2.rod_ndata()-1]);

    auto gid = ev->gid();

    events[gid] = std::move(ev);

    // Start thread to process event and send results
    process_threads.emplace_back(process, gid);
    std::cout << "Press ENTER to ask for new event" << std::endl;
    std::cin.get();
  }

  for (auto && thread : process_threads) {
    thread.join();
  }

  return 0;
}

