#!/usr/bin/env tdaq_python

from __future__ import print_function
import sys
import unittest
from HLTMPPy import HLTMPPy

class TriggerConfigJO(unittest.TestCase):
   def runTest(self):
      tc = HLTMPPy.TriggerConfigJO('myJobOptions.py',
                                   preCmds = 'x = 1',
                                   postCmds = ['a = 1','b = 2'],
                                   joType = 'MyJobOptionsSvc',
                                   pythonSetupFile = 'MyPythonSetupFile.py')

      print(HLTMPPy.prettyxml(tc.getTree()))
      sys.stdout.flush()

class TriggerConfigDB(unittest.TestCase):
   def runTest(self):
      tc = HLTMPPy.TriggerConfigDB(SMK = 1,
                                   L1PSK = 2,
                                   L1BG = 3,
                                   HPSK = 4,
                                   Coral = True,
                                   DBAlias = 'MYDB',
                                   joType = 'MyDBJobOptionsSvc')

      print(HLTMPPy.prettyxml(tc.getTree()))
      sys.stdout.flush()

class TriggerConfigDBPython(unittest.TestCase):
   def runTest(self):
      tc = HLTMPPy.TriggerConfigDBPython(SMK = 1,
                                         L1PSK = 2,
                                         L1BG = 3,
                                         HPSK = 4,
                                         Coral = True,
                                         DBAlias = 'MYDB',
                                         joType = 'MyDBJobOptionsSvc',
                                         preCmds = 'x = 1',
                                         postCmds = ['a = 1','b = 2'],
                                         pythonSetupFile = 'MyPythonDBSetupFile.py')

      print(HLTMPPy.prettyxml(tc.getTree()))
      sys.stdout.flush()

if __name__ == '__main__':
   unittest.main()
