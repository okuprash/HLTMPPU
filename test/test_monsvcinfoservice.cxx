
#define BOOST_TEST_MODULE hltmppu_test_monsvcinfoservice
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "HLTMPPU/MonSvcInfoService.h"

namespace {

// Just a unique class to use with MonSvcInfoServiceUnitTestFriendHack
struct UniqueHack {
};

}

namespace HLTMP {

template <typename T>
class MonSvcInfoServiceUnitTestFriendHack {
public:
    bool stripTag(HLTMP::MonSvcInfoService& isvc, const std::string &id, int &tag, std::string& path) {
        return isvc.stripTag(id, tag, path);
    }
};

}

using Fixture = HLTMP::MonSvcInfoServiceUnitTestFriendHack<UniqueHack>;


BOOST_FIXTURE_TEST_CASE(test_stripTag_nolbn, Fixture)
{
    // test case for stripTag() when histo name does not have LBN in it

    HLTMP::MonSvcInfoService isvc;
    int tag = 0;
    std::string path;
    bool is_lbn;

    // path and tag are not set is stripTag() returns false
    is_lbn = this->stripTag(isvc, "name", tag, path);
    BOOST_TEST(is_lbn == false);

    is_lbn = this->stripTag(isvc, "/EXPERT/name", tag, path);
    BOOST_TEST(is_lbn == false);

    is_lbn = this->stripTag(isvc, "/LB/EXPERT/name", tag, path);
    BOOST_TEST(is_lbn == false);
}

BOOST_FIXTURE_TEST_CASE(test_stripTag_lbn_run2, Fixture)
{
    // test case for stripTag() with run2 LBN convention

    HLTMP::MonSvcInfoService isvc;
    int tag = 0;
    std::string path;
    bool is_lbn;

    is_lbn = this->stripTag(isvc, "/run_0/lb_1/name", tag, path);
    BOOST_TEST(is_lbn == true);
    BOOST_TEST(path == "/name");
    BOOST_TEST(tag == 1);

    is_lbn = this->stripTag(isvc, "/run_100/lb_999/EXPERT/name", tag, path);
    BOOST_TEST(is_lbn == true);
    BOOST_TEST(path == "/EXPERT/name");
    BOOST_TEST(tag == 999);

    is_lbn = this->stripTag(isvc, "/run_999/lb_123456789/LB/EXPERT/name", tag, path);
    BOOST_TEST(is_lbn == true);
    BOOST_TEST(path == "/LB/EXPERT/name");
    BOOST_TEST(tag == 123456789);

    // some non-matching cases
    is_lbn = this->stripTag(isvc, "/run_X/lb_1/LB/EXPERT/name", tag, path);
    BOOST_TEST(is_lbn == false);

    is_lbn = this->stripTag(isvc, "/run_1/lb_Y/LB/EXPERT/name", tag, path);
    BOOST_TEST(is_lbn == false);

    is_lbn = this->stripTag(isvc, "/run_X/lb_Y/LB/EXPERT/name", tag, path);
    BOOST_TEST(is_lbn == false);

    is_lbn = this->stripTag(isvc, "/run_1/lb_-1/LB/EXPERT/name", tag, path);
    BOOST_TEST(is_lbn == false);
}

BOOST_FIXTURE_TEST_CASE(test_stripTag_lbn_run3, Fixture)
{
    // test case for stripTag() with run3 LBN convention

    HLTMP::MonSvcInfoService isvc;
    int tag = 0;
    std::string path;
    bool is_lbn;

    is_lbn = this->stripTag(isvc, "name_LB1", tag, path);
    BOOST_TEST(is_lbn == true);
    BOOST_TEST(path == "name");
    BOOST_TEST(tag == 1);

    is_lbn = this->stripTag(isvc, "name_LB1_10", tag, path);
    BOOST_TEST(is_lbn == true);
    BOOST_TEST(path == "name");
    BOOST_TEST(tag == 10);

    is_lbn = this->stripTag(isvc, "/EXPERT/name_LB999", tag, path);
    BOOST_TEST(is_lbn == true);
    BOOST_TEST(path == "/EXPERT/name");
    BOOST_TEST(tag == 999);

    is_lbn = this->stripTag(isvc, "/LB/EXPERT/name_LB999_1001", tag, path);
    BOOST_TEST(is_lbn == true);
    BOOST_TEST(path == "/LB/EXPERT/name");
    BOOST_TEST(tag == 1001);

    // some non-matching cases
    is_lbn = this->stripTag(isvc, "/LB/EXPERT/name_LBN", tag, path);
    BOOST_TEST(is_lbn == false);

    is_lbn = this->stripTag(isvc, "/LB/EXPERT/name_LBN_100", tag, path);
    BOOST_TEST(is_lbn == false);

    is_lbn = this->stripTag(isvc, "/LB/EXPERT/name_LB1_N", tag, path);
    BOOST_TEST(is_lbn == false);

    is_lbn = this->stripTag(isvc, "/LB/EXPERT/name_LB-1", tag, path);
    BOOST_TEST(is_lbn == false);

    is_lbn = this->stripTag(isvc, "/LB/EXPERT/nameLB1", tag, path);
    BOOST_TEST(is_lbn == false);
}
